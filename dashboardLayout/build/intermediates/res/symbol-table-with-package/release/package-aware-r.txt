cubi.rafi.dashboardlayout
int dimen body_padding_large 0x7f080001
int dimen body_padding_medium 0x7f080002
int dimen speaker_image_padding 0x7f080003
int dimen speaker_image_size 0x7f080004
int dimen text_size_large 0x7f080005
int dimen text_size_medium 0x7f080006
int dimen text_size_small 0x7f080007
int dimen text_size_xlarge 0x7f080008
int dimen vendor_image_size 0x7f080009
int drawable grid_menu_background 0x7f090001
int drawable grid_menu_style 0x7f090002
int style DashboardButton 0x7f160001
