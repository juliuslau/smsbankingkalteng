package bankkalteng.mobile.com.smsbanking.coreclass;

import android.content.Context;
import android.content.SharedPreferences;

import bankkalteng.mobile.com.smsbanking.R;
import timber.log.Timber;

public class PreferenceManager {
    static PreferenceManager UTILITYMANAGER = null;
    public SharedPreferences prefs;
    public SharedPreferences.Editor prefsEditor;
    private static Context context;


    public PreferenceManager(){
        context = CoreApp.getContext();
        prefs = CoreApp.getContext().getSharedPreferences(context.getString(R.string.shared_preference_name), Context.MODE_PRIVATE);
//        PREF_EDITOR = PREF.edit();
    }

    public static PreferenceManager getInstance(){
        if (UTILITYMANAGER == null)
            UTILITYMANAGER = new PreferenceManager();
        return UTILITYMANAGER;
    }

    public void clearUtilManager(){
        UTILITYMANAGER = new PreferenceManager();
    }

    public void setStr(String key, String value) {
        prefsEditor = prefs.edit();
        prefsEditor.putString(key, value);
        prefsEditor.apply();
    }

    public String getStr(String key) {
        return prefs.getString(key, "");
    }

    public void setBool(String key, boolean value) {
        prefsEditor = prefs.edit();
        prefsEditor.putBoolean(key, value);
        prefsEditor.apply();
    }
    public void setLong(String key, long value) {
        prefsEditor = prefs.edit();
        prefsEditor.putLong(key, value);
        prefsEditor.apply();
    }
    public long getLong(String key) {
        return prefs.getLong(key,0);
    }
    public boolean getBool(String key) {
        return prefs.getBoolean(key, false);
    }

    public boolean getBool(String Key, boolean defaults){
        return prefs.getBoolean(Key, defaults);
    }

    public void setInt(String key, int value) {
        prefsEditor = prefs.edit();
        prefsEditor.putInt(key, value);
        prefsEditor.apply();
    }

    public int getInt(String key) {
        return prefs.getInt(key, 0);
    }

    public void setCurrentContainer(int value){
        setInt("current_container", value);
    }

    public int getCurrentContainer(){
        return getInt("current_container");
    }


}
