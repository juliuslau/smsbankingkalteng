package bankkalteng.mobile.com.smsbanking.activities;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.text.method.KeyListener;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import bankkalteng.mobile.com.smsbanking.R;
import bankkalteng.mobile.com.smsbanking.SMSListener;
import bankkalteng.mobile.com.smsbanking.SMSReceiver;

/**
 * Created by MOBILEDEV on 1/23/2017.
 */
public class GeneralPaymentInquiryActivity extends AppCompatActivity {
    public static final String PREFS_NAME = "App_Pref";
    SharedPreferences sharedPreferences;

    Spinner spinner,spinnerTagRek;
    String[] listType={"Pilih jenis pembayaran PLN","Postpaid/PLN Taglis","PLN Non Taglis"};
    String[] listTypeTagRek={"Pilih jenis pembayaran","Tagihan Rekening Pinjaman Skedul"};
    String[] listAkun={"1","2","3","4","5"};
    AutoCompleteTextView sourceAcct;
    EditText orderId;
    String defaulIndexRek = "1";
    String responseKONTEN, paymentCategory,defaultAccountType,defaultProtocol,NO_HP,FORMAT_SMS,ORDER_ID,REK,KONTEN_SMS,KODE_BILLER;
    RelativeLayout lyt_pln,lyt_orderId,lyt_tagRek;
    int plnTypeIndexPos;
    Button btnSubmit,btnIndex;
    IntentFilter intentFilter;

    SweetAlertDialog loading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Tagihan Listrik");
        NO_HP = getResources().getString(R.string.NO_HP);
        intentFilter = new IntentFilter();
        intentFilter.addAction("SMS_RECEIVED_ACTION");
        sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        FORMAT_SMS = getResources().getString(R.string.SMS_PAYMENT);
        defaultAccountType= sharedPreferences.getString("typeAkun", "");
        defaultProtocol = sharedPreferences.getString("jalurProtocol", "");
        KeyListener keyListener = DigitsKeyListener.getInstance("1234567890");
        setContentView(R.layout.general_payment_inquiry_layout);
        spinner = (Spinner)findViewById(R.id.spinner_type);
        spinnerTagRek = (Spinner)findViewById(R.id.spinner_type_tagRek);
        orderId = (EditText)findViewById(R.id.txt_orderId); 
        sourceAcct = (AutoCompleteTextView)findViewById(R.id.autoCompleteTextView1); 
        lyt_pln = (RelativeLayout)findViewById(R.id.lyt_type_pln);
        lyt_tagRek= (RelativeLayout)findViewById(R.id.lyt_type_tagRek);
        lyt_orderId  = (RelativeLayout)findViewById(R.id.lyt_orderId);
        btnSubmit = (Button)findViewById(R.id.btnSubmit);
        sourceAcct.setKeyListener(keyListener);
        orderId.setKeyListener(keyListener);
        btnIndex = (Button)findViewById(R.id.btnIndex);


        if(defaultAccountType.equalsIgnoreCase("Satu Akun")){
            sourceAcct.setFilters(new InputFilter[]{new InputFilter.LengthFilter(35) {

            }});
            sourceAcct.setText("index rekening 1");
            sourceAcct.setTextSize(14);
            sourceAcct.setClickable(false);
            sourceAcct.setCursorVisible(false);
            sourceAcct.setEnabled(false);

        }else{
            ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,listAkun);
            sourceAcct.setAdapter(adapter);
            sourceAcct.setThreshold(1);
            sourceAcct.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1) {

            }});
        }

        Intent intent = getIntent();
        paymentCategory= intent.getStringExtra("paymentCategory");
        if(paymentCategory.equalsIgnoreCase("Tagihan Listrik")){
            lyt_tagRek.setVisibility(View.GONE);
            lyt_pln.setVisibility(View.VISIBLE);
            spinner = (Spinner)findViewById(R.id.spinner_type);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item,R.id.textview,listType);
            spinner.setAdapter(adapter);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    plnTypeIndexPos = spinner.getSelectedItemPosition();


                    try{
                        if(plnTypeIndexPos == 1){
                            KODE_BILLER = getResources().getString(R.string.SMS_PEMBELIAN_PULSA_LISTRIK);
                        }if(plnTypeIndexPos == 2){
                            KODE_BILLER = getResources().getString(R.string.SMS_PEMBAYARAN_NONTAGLIS);
                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }else{

            lyt_pln.setVisibility(View.GONE);
            lyt_tagRek.setVisibility(View.VISIBLE);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Tagihan Rekening Pinjaman");
            spinnerTagRek = (Spinner)findViewById(R.id.spinner_type_tagRek);
            orderId.setHint("masukan no rekening pinjaman");
            orderId.setFilters(new InputFilter[]{new InputFilter.LengthFilter(16)});
            orderId.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                    getResources().getDimension(R.dimen.hint_editext));
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item,R.id.textview,listTypeTagRek);
            spinnerTagRek.setAdapter(adapter);

            spinnerTagRek.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    plnTypeIndexPos = spinnerTagRek.getSelectedItemPosition();

                    try{
                        if(plnTypeIndexPos == 1){
                            KODE_BILLER = getResources().getString(R.string.SMS_PEMBAYARAN_REKENING_PINJAMAN);

                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });


        }

        btnIndex.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                KONTEN_SMS = getResources().getString(R.string.SMS_INDEX_REKENING);
                sendSMS(NO_HP,KONTEN_SMS);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (defaultAccountType.equalsIgnoreCase("Satu Akun")) {
                        if (defaultProtocol.equalsIgnoreCase("SMS")) {
                                if(plnTypeIndexPos == 0) {
                                SweetAlertDialog loading = new SweetAlertDialog(GeneralPaymentInquiryActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Silahkan pilih jenis pembayaran !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                return;

                            }if(orderId.getText().toString().equalsIgnoreCase("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(GeneralPaymentInquiryActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan ID pelanggan !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                orderId.setError("");
                                return;

                            }if(orderId.getText().toString().length() < 6 || orderId.getText().toString().length() > 16) {
                                SweetAlertDialog loading = new SweetAlertDialog(GeneralPaymentInquiryActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan ID pelanggan dengan benar !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                orderId.setError("");
                                return;

                            }else{
                                REK = defaulIndexRek;
                                ORDER_ID = orderId.getText().toString();
                                KONTEN_SMS = FORMAT_SMS + " " + KODE_BILLER+ " " + REK + " " + ORDER_ID;
                                sendSMS(NO_HP,KONTEN_SMS);


                            }

                        } else {

                        }
                    } else {
                        if (defaultProtocol.equalsIgnoreCase("SMS")) {
                            if (sourceAcct.getText().toString().equalsIgnoreCase("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(GeneralPaymentInquiryActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan index rekening !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                sourceAcct.setError("");
                                return;
                            }if(plnTypeIndexPos == 0) {
                                SweetAlertDialog loading = new SweetAlertDialog(GeneralPaymentInquiryActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Silahkan pilih jenis pembayaran !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                return;

                            }if(orderId.getText().toString().equalsIgnoreCase("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(GeneralPaymentInquiryActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan ID pelanggan !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                orderId.setError("");
                                return;

                            }if(orderId.getText().toString().length() < 11) {
                                SweetAlertDialog loading = new SweetAlertDialog(GeneralPaymentInquiryActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan ID pelanggan dengan benar !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                orderId.setError("");
                                return;

                            }else{
                                REK = sourceAcct.getText().toString();
                                ORDER_ID = orderId.getText().toString();
                                KONTEN_SMS = FORMAT_SMS + " " + KODE_BILLER + " " + REK + " " + ORDER_ID;
                                sendSMS(NO_HP,KONTEN_SMS);

                            }
                        } else {

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        SMSReceiver.bindListener(new SMSListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void messageReceived(String messageText) {

                final Matcher matcher = Pattern.compile("disertai digit").matcher(messageText);
                final Matcher matcherIndex = Pattern.compile("Index Rekening").matcher(messageText);
                if(matcher.find()){
                    AlertDialog.Builder alert = new AlertDialog.Builder(GeneralPaymentInquiryActivity.this);
                    alert.setMessage(messageText); //Message here

                    LinearLayout layout = new LinearLayout(GeneralPaymentInquiryActivity.this);
                    layout.setOrientation(LinearLayout.VERTICAL);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(20, 0, 30, 0);

                    final EditText input = new EditText(GeneralPaymentInquiryActivity.this);
                    layout.addView(input,params);
                    input.setInputType(InputType.TYPE_CLASS_TEXT);


                    input.setMaxLines(5);
                    input.setMaxWidth(50);
                    input.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5) {

                    }});


                    alert.setView(layout);


                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            responseKONTEN = input.getText().toString();

                            if (input.getText().length()==0){
                                input.setError("masukan index rekening");
                                return;
                            }else {

                                sendSMS(NO_HP,responseKONTEN);
                                return;
                            }


                        } // End of onClick(DialogInterface dialog, int whichButton)
                    }); //End of alert.setPositiveButton


                    alert.setCancelable(false);
                    alert.show();
                    return;

                }else{
                    new SweetAlertDialog(GeneralPaymentInquiryActivity.this, SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText("")
                            .setContentText(messageText)
                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    return;
                }

            }
        });

    }




    public void ClearField(){
        if(defaultAccountType.equalsIgnoreCase("Satu Akun")){
            sourceAcct.setFilters(new InputFilter[]{new InputFilter.LengthFilter(35) {

            }});
            sourceAcct.setText("Anda menggunakan index rekening 1");
            sourceAcct.setTextSize(12);
            sourceAcct.setClickable(false);
            sourceAcct.setCursorVisible(false);
            sourceAcct.setEnabled(false);
        }else{
            sourceAcct.getText().clear();
        }
        orderId.getText().clear();
        

    }



    @Override
    protected void onPause() {
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        registerReceiver(sendBroadcastReceiver,intentFilter);
        super.onResume();
    }

    @Override
    protected void onDestroy() {

        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        super.onDestroy();
    }


    private void sendSMS(String phoneNumber, String message) {

        loading = new SweetAlertDialog(GeneralPaymentInquiryActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setTitleText("Memproses...");

        loading.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                text.setGravity(Gravity.CENTER);
            }
        });
        loading.show();


        new CountDownTimer(30000, 1000) { //40000 milli seconds is total time, 1000 milli seconds is time interval

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                loading.dismiss();
            }
        }.start();




        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
                SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

        registerReceiver(deliverBroadcastReceiver, new IntentFilter(DELIVERED));
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
        ClearField();


    }

    BroadcastReceiver sendBroadcastReceiver = new SentReceiver();
    BroadcastReceiver deliverBroadcastReceiver = new DeliverReceiver();


    class DeliverReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    loading.dismiss();
                    break;
                case Activity.RESULT_CANCELED:
                    loading.dismiss();
                    Toast.makeText(getBaseContext(), "Gagal Mengirim",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }


    class SentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "TERKIRIM", Toast.LENGTH_SHORT)
                            .show();
                    loading.dismiss();
                    new SweetAlertDialog(GeneralPaymentInquiryActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Perhatian !")
                            .setContentText("Mohon menunggu sampai anda mendapat informasi balasan dari kami !")

                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    loading.dismiss();
                    Toast.makeText(getBaseContext(), "Generic failure",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    loading.dismiss();
                    Toast.makeText(getBaseContext(), "No service",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    loading.dismiss();
                    Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT)
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    loading.dismiss();
                    Toast.makeText(getBaseContext(), "Radio off",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }




}
