package bankkalteng.mobile.com.smsbanking.activities;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.InputFilter;
import android.text.InputType;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import bankkalteng.mobile.com.smsbanking.AppHelper;
import bankkalteng.mobile.com.smsbanking.R;
import bankkalteng.mobile.com.smsbanking.SMSListener;
import bankkalteng.mobile.com.smsbanking.SMSReceiver;

/**
 * Created by MOBILEDEV on 10/27/2016.
 */
public class TransferActivity extends AppCompatActivity {
    Button btnInhouse, btnDomestic;
    public String defaultProtocol,ussdCode;
    public String NO_HP, KONTEN_SMS,PIN, KODE_REK,USER_PIN,responseKONTEN;
    MainActivity mainActivity;

    IntentFilter intentFilter;

    SweetAlertDialog loading;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.transfer_layout);

        btnInhouse = (Button)findViewById(R.id.menu_transfer_inhouse);
        btnDomestic = (Button)findViewById(R.id.menu_transfer_domestic);

        intentFilter = new IntentFilter();
        intentFilter.addAction("SMS_RECEIVED_ACTION");
        Animation anim_left = AnimationUtils.loadAnimation(getApplication(), R.anim.push_left);
        Animation anim_right = AnimationUtils.loadAnimation(getApplication(), R.anim.push_right);
        btnInhouse.startAnimation(anim_left);
        btnDomestic.startAnimation(anim_right);
        KODE_REK = getResources().getString(R.string.KODE_REK);
        NO_HP = getResources().getString(R.string.NO_HP);
        USER_PIN = AppHelper.getPIN(getApplication());
        btnDomestic.setOnClickListener(myButtonListener);
        btnInhouse.setOnClickListener(myButtonListener);
        defaultProtocol = AppHelper.getProtocol(getApplicationContext());


        SMSReceiver.bindListener(new SMSListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void messageReceived(String messageText) {

                final Matcher matcher = Pattern.compile("disertai digit").matcher(messageText);
                if(matcher.find()){
                    AlertDialog.Builder alert = new AlertDialog.Builder(TransferActivity.this);
                    alert.setMessage(messageText); //Message here

                    LinearLayout layout = new LinearLayout(TransferActivity.this);
                    layout.setOrientation(LinearLayout.VERTICAL);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(20, 0, 30, 0);

                    final EditText input = new EditText(TransferActivity.this);
                    layout.addView(input,params);
                    input.setInputType(InputType.TYPE_CLASS_TEXT);


                    input.setMaxLines(5);
                    input.setMaxWidth(50);
                    input.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5) {

                    }});


                    alert.setView(layout);


                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            responseKONTEN = input.getText().toString();

                            if (input.getText().length()==0){
                                input.setError("");
                                return;
                            }else {

                                sendSMS(NO_HP,responseKONTEN);
                                return;
                            }


                        } // End of onClick(DialogInterface dialog, int whichButton)
                    }); //End of alert.setPositiveButton


                    alert.setCancelable(false);
                    alert.show();

                }else{

                    new SweetAlertDialog(TransferActivity.this, SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText("")
                            .setContentText(messageText)
                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                }

            }
        });



    }

    private View.OnClickListener myButtonListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.menu_transfer_inhouse:

                    Intent intent = new Intent(TransferActivity.this, TransferInhouseIndex.class);
                    startActivity(intent);
                    break;
                case R.id.menu_transfer_domestic:
                    intent = new Intent(TransferActivity.this, TransferDomesticActivity.class);
                    startActivity(intent);
                    break;
            }
        }
    };


    private void showPinUnlockScreen() {
        Intent intent = new Intent(this, PinUnlockActivity.class);
        intent.putExtra("NO_HP", NO_HP);
        intent.putExtra("PIN", USER_PIN);
        intent.putExtra("KODE_REK", KODE_REK);
        intent.putExtra("KONTEN_SMS", KONTEN_SMS);
        startActivity(intent);



    }

    public void showPINSetupScreen() {
//        Intent intent = new Intent(TransferActivity.this, PinSetupActivity.class);
//        startActivity(intent);
    }



    private void sendSMS(String phoneNumber, String message) {

        loading = new SweetAlertDialog(TransferActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setTitleText("Memproses...");

        loading.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                text.setGravity(Gravity.CENTER);
            }
        });
        loading.show();


        new CountDownTimer(30000, 1000) { //40000 milli seconds is total time, 1000 milli seconds is time interval

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                loading.dismiss();
            }
        }.start();




        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
                SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

        registerReceiver(deliverBroadcastReceiver, new IntentFilter(DELIVERED));
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);



    }

    BroadcastReceiver sendBroadcastReceiver = new SentReceiver();
    BroadcastReceiver deliverBroadcastReceiver = new DeliverReceiver();


    class DeliverReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:

                    loading.dismiss();
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(getBaseContext(), "Gagal Terkirimm",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }


    class SentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "Mengirim", Toast.LENGTH_SHORT)
                            .show();
                    loading.dismiss();
                    new SweetAlertDialog(TransferActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Perhatian !")
                            .setContentText("Mohon menunggu sampai anda mendapat informasi balasan dari kami !")

                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Toast.makeText(getBaseContext(), "Generic failure",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Toast.makeText(getBaseContext(), "No service",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT)
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Toast.makeText(getBaseContext(), "Radio off",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }


    @Override
    protected void onResume() {

        registerReceiver(sendBroadcastReceiver,intentFilter);

        super.onPostResume();
    }

    @Override
    protected void onPause() {
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
