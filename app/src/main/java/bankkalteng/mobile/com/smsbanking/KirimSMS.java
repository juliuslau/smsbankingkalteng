package bankkalteng.mobile.com.smsbanking;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;

/**
 * Created by rpj on 07/09/17.
 */

public class KirimSMS {

    String noHP;
    String kontenSMS;

    Context context;

    public KirimSMS(String noHP, String kontenSMS) {
        this.noHP = noHP;
        this.kontenSMS = kontenSMS;

        String SENT = "Mengirim";
        String DELIVERED = "Terkirim";

        PendingIntent sentPI = PendingIntent.getBroadcast(context,0,new Intent(SENT),0);
        PendingIntent deliverPI = PendingIntent.getBroadcast(context,0,new Intent(DELIVERED),0);

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(noHP, null, kontenSMS, sentPI, deliverPI);
    }
}
