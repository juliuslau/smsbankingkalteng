package bankkalteng.mobile.com.smsbanking.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.FrameLayout;

import java.util.Locale;

import bankkalteng.mobile.com.smsbanking.R;
import bankkalteng.mobile.com.smsbanking.fragments.FragmentFAQWeb;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityFaqWeb extends AppCompatActivity {

    @BindView(R.id.main_fragment)
    FrameLayout content;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq_web);
        ButterKnife.bind(this);
        InitializeDashboard(new FragmentFAQWeb(),"Fragment Faq Web");
    }

    public void InitializeDashboard(Fragment frag, String tag){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(content.getId(), frag, tag);
        ft.commitAllowingStateLoss();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(getSupportFragmentManager().getBackStackEntryCount() > 0 ){
            getSupportFragmentManager().popBackStack();
        }else
            finish();

    }
}
