package bankkalteng.mobile.com.smsbanking.activities;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.text.method.KeyListener;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import bankkalteng.mobile.com.smsbanking.R;
import bankkalteng.mobile.com.smsbanking.SMSListener;
import bankkalteng.mobile.com.smsbanking.SMSReceiver;

/**
 * Created by MOBILEDEV on 12/7/2016.
 */
public class TransferInhouseIndex extends AppCompatActivity {
    public static final String PREFS_NAME = "App_Pref";
    AutoCompleteTextView sourceAccount, benefAccount;
    EditText nominal,berita;
    Button btnSubmit,btnIndex;
    SharedPreferences sharedPreferences;
    private ProgressDialog pDialog;
    String defaultAccountType,defaultProtocol;
    String INDEX_REK1, INDEX_REK2, KONTEN_SMS,NOMINAL, NO_HP,FORMAT_SMS,ussdCode,BERITA,sourceAccts;
    RelativeLayout lyt_1, lyt_2, lyt_3, lyt_4;
    public String responseKONTEN;
    TextView lbl_index_rek;

    String[] sourceAcct={"2","3","4","5"};
    String[] BenefAcct={"1","2","3","4","5"};

    IntentFilter intentFilter;

    SweetAlertDialog loading;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inhouse_index_layout);
        intentFilter = new IntentFilter();
        intentFilter.addAction("SMS_RECEIVED_ACTION");
        NO_HP =getResources().getString(R.string.NO_HP);
        FORMAT_SMS = getResources().getString(R.string.SMS_TRANSFER_INHOUSE);
        sourceAccount=(AutoCompleteTextView)findViewById(R.id.autoCompleteTextView1);
        benefAccount=(AutoCompleteTextView)findViewById(R.id.autoCompleteTextView2);
        sourceAccount.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        });
        nominal =(EditText)findViewById(R.id.txt_nominal);
        berita = (EditText)findViewById(R.id.txt_berita);
        lyt_1 = (RelativeLayout)findViewById(R.id.lyt_1);
        lyt_4 = (RelativeLayout)findViewById(R.id.lyt_4);
        lbl_index_rek = (TextView)findViewById(R.id.lbl_index_rek);

        btnIndex = (Button)findViewById(R.id.btnIndex);
        btnSubmit = (Button)findViewById(R.id.btnSubmit);
        sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        defaultAccountType= sharedPreferences.getString("typeAkun", "");
        defaultProtocol = sharedPreferences.getString("jalurProtocol", "");
        if(defaultAccountType.equalsIgnoreCase("Satu Akun")){
            sourceAccount.setFilters(new InputFilter[]{new InputFilter.LengthFilter(35) {

            }});
            sourceAccount.setText("index rekening 1");
            sourceAccount.setTextSize(14);
            sourceAccount.setClickable(false);
            sourceAccount.setCursorVisible(false);
            sourceAccount.setEnabled(false);
        }else{
            ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,sourceAcct);
            sourceAccount.setAdapter(adapter);
            sourceAccount.setThreshold(1);
            sourceAccount.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1) {

            }});
        }



        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,sourceAcct);
        sourceAccount.setAdapter(adapter);
        sourceAccount.setThreshold(1);

        ArrayAdapter adapters = new ArrayAdapter(this,android.R.layout.simple_list_item_1,BenefAcct);
        benefAccount.setAdapter(adapters);
        benefAccount.setThreshold(1);

        sourceAccount.setInputType(InputType.TYPE_CLASS_PHONE);
        benefAccount.setInputType(InputType.TYPE_CLASS_PHONE);
        nominal.setInputType(InputType.TYPE_CLASS_PHONE);

        KeyListener keyListener = DigitsKeyListener.getInstance("1234567890");
        sourceAccount.setKeyListener(keyListener);
        benefAccount.setKeyListener(keyListener);
        nominal.setKeyListener(keyListener);

        final InputMethodManager imm = (InputMethodManager)getApplication().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(nominal.getWindowToken(), 0);
        sourceAccount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                imm.showSoftInputFromInputMethod(nominal.getWindowToken(), 0);
            }
        });
        benefAccount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                imm.showSoftInputFromInputMethod(nominal.getWindowToken(), 0);
            }
        });



        nominal.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                ClipboardManager cm = (ClipboardManager)getApplicationContext().getSystemService(Context.CLIPBOARD_SERVICE);
                cm.setText(nominal.getText());
                Toast.makeText(getApplicationContext(), "Copied to clipboard", Toast.LENGTH_SHORT).show();

                return false;
            }
        });

        btnIndex.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                KONTEN_SMS = getResources().getString(R.string.SMS_INDEX_REKENING);
                sendSMS(NO_HP,KONTEN_SMS);
            }
        });


        SMSReceiver.bindListener(new SMSListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void messageReceived(String messageText) {

                final Matcher matcher = Pattern.compile("disertai digit").matcher(messageText);

                if(matcher.find()){
                    AlertDialog.Builder alert = new AlertDialog.Builder(TransferInhouseIndex.this);
                    alert.setMessage(messageText); //Message here

                    LinearLayout layout = new LinearLayout(TransferInhouseIndex.this);
                    layout.setOrientation(LinearLayout.VERTICAL);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(20, 0, 30, 0);

                    final EditText input = new EditText(TransferInhouseIndex.this);
                    layout.addView(input,params);
                    input.setInputType(InputType.TYPE_CLASS_TEXT);


                    input.setMaxLines(5);
                    input.setMaxWidth(50);
                    input.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5) {

                    }});


                    alert.setView(layout);


                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            responseKONTEN = input.getText().toString();

                            if (input.getText().length()==0){
                                input.setError("");
                                return;
                            }else {

                                sendSMS(NO_HP,responseKONTEN);
                                return;
                            }


                        } // End of onClick(DialogInterface dialog, int whichButton)
                    }); //End of alert.setPositiveButton


                    alert.setCancelable(true);
                    alert.show();
                    return;

                }else{
                    new SweetAlertDialog(TransferInhouseIndex.this, SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText("")
                            .setContentText(messageText)
                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    return;
                }

            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if(defaultAccountType.equalsIgnoreCase("Satu Akun")){
                        if(defaultProtocol.equalsIgnoreCase("SMS")){
                            if (benefAccount.getText().toString().equals("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(TransferInhouseIndex.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan rekening tujuan !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                benefAccount.setError("");
                                return;
                            }
                            if (benefAccount.getText().toString().length() < 1 || benefAccount.getText().toString().length() > 16){
                                SweetAlertDialog loading = new SweetAlertDialog(TransferInhouseIndex.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan rekening tujuan dengan benar !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                benefAccount.setError("");
                                return;
                            }if (nominal.getText().toString().equals("")){
                                SweetAlertDialog loading = new SweetAlertDialog(TransferInhouseIndex.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan jumlah transfer !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                nominal.setError("");
                                return;
                            }else {
                                BERITA = berita.getText().toString();
                                INDEX_REK2 = benefAccount.getText().toString();
                                NOMINAL = nominal.getText().toString();
                                KONTEN_SMS = FORMAT_SMS + " " + "1" + " " + INDEX_REK2 + " " + NOMINAL + "#" + BERITA ;
                                sendSMS(NO_HP,KONTEN_SMS);
                            }
                        }else{
                            ussdCode = "*" + "141" + "*" + "3" + "*" + "1" + "*" + INDEX_REK2 + "*" + NOMINAL +  Uri.encode("#");
                            if (ActivityCompat.checkSelfPermission(TransferInhouseIndex.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            if (ActivityCompat.checkSelfPermission(TransferInhouseIndex.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
                        }
                
                        
                    }else{
                        if(defaultProtocol.equalsIgnoreCase("SMS")) {
                            if (sourceAccount.getText().toString().equals("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(TransferInhouseIndex.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan index rekening !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                sourceAccount.setError("");
                                return;
                            }
                            if (benefAccount.getText().toString().equals("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(TransferInhouseIndex.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan rekening tujuan !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                benefAccount.setError("");
                                return;
                            }
                            if (nominal.getText().toString().equals("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(TransferInhouseIndex.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan jumlah transfer !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                nominal.setError("");
                                return;

                            }else {
                                INDEX_REK1 = sourceAccount.getText().toString();
                                INDEX_REK2 = benefAccount.getText().toString();
                                BERITA = berita.getText().toString();
                                NOMINAL = nominal.getText().toString();
                                KONTEN_SMS = FORMAT_SMS + " " + INDEX_REK1 + " " + INDEX_REK2 + " " +NOMINAL+"#"+BERITA;
                                sendSMS(NO_HP,KONTEN_SMS);
                            }
                        }else{
                            ussdCode = "*" + "141" + "*" + "125" + Uri.encode("#");
                            if (ActivityCompat.checkSelfPermission(TransferInhouseIndex.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            if (ActivityCompat.checkSelfPermission(TransferInhouseIndex.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
                        }
                    }
                   
                }catch (Exception e){
                    e.printStackTrace();
                }



            }

        });



    }


    @Override
    protected void onPause() {
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {

        registerReceiver(sendBroadcastReceiver,intentFilter);

        super.onResume();
    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        super.onDestroy();
    }

    public void ClearField(){
        if(defaultAccountType.equalsIgnoreCase("Satu Akun")){
            sourceAccount.setFilters(new InputFilter[]{new InputFilter.LengthFilter(35) {

            }});
            sourceAccount.setText("index rekening 1");
            sourceAccount.setTextSize(12);
            sourceAccount.setClickable(false);
            sourceAccount.setCursorVisible(false);
            sourceAccount.setEnabled(false);
        }else{
            sourceAccount.getText().clear();
        }
        benefAccount.getText().clear();
        nominal.getText().clear();
        berita.getText().clear();
    }






    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id != null) {
//            return true;
//        }
        return super.onOptionsItemSelected(item);
    }

    private void sendSMS(String phoneNumber, String message) {

        loading = new SweetAlertDialog(TransferInhouseIndex.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setTitleText("Memproses...");

        loading.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                text.setGravity(Gravity.CENTER);
            }
        });
        loading.show();


        new CountDownTimer(30000, 1000) { //40000 milli seconds is total time, 1000 milli seconds is time interval

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                loading.dismiss();
            }
        }.start();




        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
                SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

        registerReceiver(deliverBroadcastReceiver, new IntentFilter(DELIVERED));
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
        ClearField();


    }

    BroadcastReceiver sendBroadcastReceiver = new SentReceiver();
    BroadcastReceiver deliverBroadcastReceiver = new DeliverReceiver();


    class DeliverReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    loading.dismiss();
                    break;
                case Activity.RESULT_CANCELED:
                    loading.dismiss();
                    Toast.makeText(getBaseContext(), "Gagal Mengirim",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }


    class SentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "TERKIRIM", Toast.LENGTH_SHORT)
                            .show();
                    loading.dismiss();
                    new SweetAlertDialog(TransferInhouseIndex.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Perhatian !")
                            .setContentText("Mohon menunggu sampai anda mendapat informasi balasan dari kami !")

                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    loading.dismiss();
                    Toast.makeText(getBaseContext(), "Generic failure",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    loading.dismiss();
                    Toast.makeText(getBaseContext(), "No service",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    loading.dismiss();
                    Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT)
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    loading.dismiss();
                    Toast.makeText(getBaseContext(), "Radio off",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }


}
