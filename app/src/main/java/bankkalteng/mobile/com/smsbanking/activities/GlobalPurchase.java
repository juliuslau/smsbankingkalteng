package bankkalteng.mobile.com.smsbanking.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import bankkalteng.mobile.com.smsbanking.R;

/**
 * Created by MOBILEDEV on 1/19/2017.
 */
public class GlobalPurchase extends AppCompatActivity {
    public static final String PREFS_NAME = "App_Pref";
    public String purchaseCategory,defaultAccountType,defaultProtocol,FORMAT_SMS,NO_HP;
    SharedPreferences sharedPreferences;
    RelativeLayout lyt_1,lyt_pulsa, lyt_2, lyt_3;
    TextView lbl_index_rek;
    ListView listItemPulsa;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.global_purchase_layout);
        sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        FORMAT_SMS = getResources().getString(R.string.SMS_TRANSFER_DOMESTIC);
        NO_HP =getResources().getString(R.string.NO_HP);
        Intent intent = getIntent();
        purchaseCategory = intent.getStringExtra("purchaseCategory");
        defaultAccountType= sharedPreferences.getString("typeAkun", "");
        defaultProtocol = sharedPreferences.getString("jalurProtocol", "");
        listItemPulsa = (ListView)findViewById(R.id.list_item_pulsa);
        lyt_1 = (RelativeLayout)findViewById(R.id.lyt_1);
        lyt_pulsa = (RelativeLayout)findViewById(R.id.lyt_pulsa);
        lbl_index_rek = (TextView)findViewById(R.id.lbl_index_rek);

        lyt_pulsa.setVisibility(View.GONE);

        try{
            if(purchaseCategory.equalsIgnoreCase("Pulsa")){
                lyt_pulsa.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){

        }




        if(defaultAccountType.equalsIgnoreCase("Satu Akun")){
            lyt_1.setVisibility(View.GONE);
            lyt_1.setClickable(false);
            lbl_index_rek.setVisibility(View.VISIBLE);
            lbl_index_rek.setText("INDEX REKENING = 1");
        }else{
            lyt_1.setVisibility(View.VISIBLE);
            lyt_1.setClickable(true);
            lbl_index_rek.setVisibility(View.GONE);
        }

    }
}
