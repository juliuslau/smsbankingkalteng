//package bankkalteng.mobile.com.smsbanking.mobile.com.smsbanking.activities;
//
//import android.app.Service;
//import android.content.Context;
//import android.graphics.drawable.Drawable;
//import android.net.Uri;
//import android.os.Build;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v7.app.AppCompatActivity;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.util.AttributeSet;
//import android.util.Log;
//import android.view.KeyEvent;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.EditText;
//import android.widget.LinearLayout;
//
//import com.google.android.gms.appindexing.Action;
//import com.google.android.gms.appindexing.AppIndex;
//import com.google.android.gms.common.api.GoogleApiClient;
//
//import bankkalteng.mobile.com.smsbanking.mobile.com.smsbanking.R;
//
///**
// * Created by MOBILEDEV on 10/30/2016.
// */
//public class InputPinActivity extends AppCompatActivity implements View.OnFocusChangeListener, View.OnKeyListener, TextWatcher {
//    private EditText mPinFirstDigitEditText;
//    private EditText mPinSecondDigitEditText;
//    private EditText mPinThirdDigitEditText;
//    private EditText mPinForthDigitEditText;
//    private EditText mPinFifthDigitEditText;
//    private EditText mPinHiddenEditText;
//    /**
//     * ATTENTION: This was auto-generated to implement the App Indexing API.
//     * See https://g.co/AppIndexing/AndroidStudio for more information.
//     */
//    private GoogleApiClient client;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(new MainLayout(this, null));
//        // ATTENTION: This was auto-generated to implement the App Indexing API.
//        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
//    }
//
//    @Override
//    public void onFocusChange(View v, boolean hasFocus) {
//        final int id = v.getId();
//        switch (id) {
//            case R.id.pin_first_edittext:
//                if (hasFocus) {
//                    setFocus(mPinHiddenEditText);
//                    showSoftKeyboard(mPinHiddenEditText);
//                }
//                break;
//
//            case R.id.pin_second_edittext:
//                if (hasFocus) {
//                    setFocus(mPinHiddenEditText);
//                    showSoftKeyboard(mPinHiddenEditText);
//                }
//                break;
//
//            case R.id.pin_third_edittext:
//                if (hasFocus) {
//                    setFocus(mPinHiddenEditText);
//                    showSoftKeyboard(mPinHiddenEditText);
//                }
//                break;
//
//            case R.id.pin_forth_edittext:
//                if (hasFocus) {
//                    setFocus(mPinHiddenEditText);
//                    showSoftKeyboard(mPinHiddenEditText);
//                }
//                break;
//
//            case R.id.pin_fifth_edittext:
//                if (hasFocus) {
//                    setFocus(mPinHiddenEditText);
//                    showSoftKeyboard(mPinHiddenEditText);
//                }
//                break;
//            default:
//                break;
//        }
//    }
//
//    @Override
//    public boolean onKey(View v, int keyCode, KeyEvent event) {
//        if (event.getAction() == KeyEvent.ACTION_DOWN) {
//            final int id = v.getId();
//            switch (id) {
//                case R.id.pin_hidden_edittext:
//                    if (keyCode == KeyEvent.KEYCODE_DEL) {
//                        if (mPinHiddenEditText.getText().length() == 5)
//                            mPinFifthDigitEditText.setText("");
//                        else if (mPinHiddenEditText.getText().length() == 4)
//                            mPinForthDigitEditText.setText("");
//                        else if (mPinHiddenEditText.getText().length() == 3)
//                            mPinThirdDigitEditText.setText("");
//                        else if (mPinHiddenEditText.getText().length() == 2)
//                            mPinSecondDigitEditText.setText("");
//                        else if (mPinHiddenEditText.getText().length() == 1)
//                            mPinFirstDigitEditText.setText("");
//
//                        if (mPinHiddenEditText.length() > 0)
//                            mPinHiddenEditText.setText(mPinHiddenEditText.getText().subSequence(0, mPinHiddenEditText.length() - 1));
//
//                        return true;
//                    }
//
//                    break;
//
//                default:
//                    return false;
//            }
//        }
//
//        return false;
//    }
//
//    @Override
//    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//    }
//
//    @Override
//    public void onTextChanged(CharSequence s, int start, int before, int count) {
//        setDefaultPinBackground(mPinFirstDigitEditText);
//        setDefaultPinBackground(mPinSecondDigitEditText);
//        setDefaultPinBackground(mPinThirdDigitEditText);
//        setDefaultPinBackground(mPinForthDigitEditText);
//        setDefaultPinBackground(mPinFifthDigitEditText);
//
//        if (s.length() == 0) {
//            setFocusedPinBackground(mPinFirstDigitEditText);
//            mPinFirstDigitEditText.setText("");
//        } else if (s.length() == 1) {
//            setFocusedPinBackground(mPinSecondDigitEditText);
//            mPinFirstDigitEditText.setText(s.charAt(0) + "");
//            mPinSecondDigitEditText.setText("");
//            mPinThirdDigitEditText.setText("");
//            mPinForthDigitEditText.setText("");
//            mPinFifthDigitEditText.setText("");
//        } else if (s.length() == 2) {
//            setFocusedPinBackground(mPinThirdDigitEditText);
//            mPinSecondDigitEditText.setText(s.charAt(1) + "");
//            mPinThirdDigitEditText.setText("");
//            mPinForthDigitEditText.setText("");
//            mPinFifthDigitEditText.setText("");
//        } else if (s.length() == 3) {
//            setFocusedPinBackground(mPinForthDigitEditText);
//            mPinThirdDigitEditText.setText(s.charAt(2) + "");
//            mPinForthDigitEditText.setText("");
//            mPinFifthDigitEditText.setText("");
//        } else if (s.length() == 4) {
//            setFocusedPinBackground(mPinFifthDigitEditText);
//            mPinForthDigitEditText.setText(s.charAt(3) + "");
//            mPinFifthDigitEditText.setText("");
//        } else if (s.length() == 5) {
//            setDefaultPinBackground(mPinFifthDigitEditText);
//            mPinFifthDigitEditText.setText(s.charAt(4) + "");
//
//            hideSoftKeyboard(mPinFifthDigitEditText);
//        }
//    }
//
//    @Override
//    public void afterTextChanged(Editable editable) {
//
//    }
//
//    public void hideSoftKeyboard(EditText editText) {
//        if (editText == null)
//            return;
//
//        InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
//        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
//    }
//
//    private void init() {
//        mPinFirstDigitEditText = (EditText) findViewById(R.id.pin_first_edittext);
//        mPinSecondDigitEditText = (EditText) findViewById(R.id.pin_second_edittext);
//        mPinThirdDigitEditText = (EditText) findViewById(R.id.pin_third_edittext);
//        mPinForthDigitEditText = (EditText) findViewById(R.id.pin_forth_edittext);
//        mPinFifthDigitEditText = (EditText) findViewById(R.id.pin_fifth_edittext);
//        mPinHiddenEditText = (EditText) findViewById(R.id.pin_hidden_edittext);
//    }
//
//    private void setDefaultPinBackground(EditText editText) {
//        setViewBackground(editText, getResources().getDrawable(R.drawable.edit_text_holo_light));
//    }
//
//    public static void setFocus(EditText editText) {
//        if (editText == null)
//            return;
//
//        editText.setFocusable(true);
//        editText.setFocusableInTouchMode(true);
//        editText.requestFocus();
//    }
//
//    private void setFocusedPinBackground(EditText editText) {
//        setViewBackground(editText, getResources().getDrawable(R.drawable.textfield_focused_holo_light));
//    }
//
//    private void setPINListeners() {
//        mPinHiddenEditText.addTextChangedListener(this);
//
//        mPinFirstDigitEditText.setOnFocusChangeListener(this);
//        mPinSecondDigitEditText.setOnFocusChangeListener(this);
//        mPinThirdDigitEditText.setOnFocusChangeListener(this);
//        mPinForthDigitEditText.setOnFocusChangeListener(this);
//        mPinFifthDigitEditText.setOnFocusChangeListener(this);
//
//        mPinFirstDigitEditText.setOnKeyListener(this);
//        mPinSecondDigitEditText.setOnKeyListener(this);
//        mPinThirdDigitEditText.setOnKeyListener(this);
//        mPinForthDigitEditText.setOnKeyListener(this);
//        mPinFifthDigitEditText.setOnKeyListener(this);
//        mPinHiddenEditText.setOnKeyListener(this);
//    }
//
//    @SuppressWarnings("deprecation")
//    public void setViewBackground(View view, Drawable background) {
//        if (view == null || background == null)
//            return;
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//            view.setBackground(background);
//        } else {
//            view.setBackgroundDrawable(background);
//        }
//    }
//
//    public void showSoftKeyboard(EditText editText) {
//        if (editText == null)
//            return;
//
//        InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
//        imm.showSoftInput(editText, 0);
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//
//        // ATTENTION: This was auto-generated to implement the App Indexing API.
//        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        client.connect();
//        Action viewAction = Action.newAction(
//                Action.TYPE_VIEW, // TODO: choose an action type.
//                "InputPin Page", // TODO: Define a title for the content shown.
//                // TODO: If you have web page content that matches this app activity's content,
//                // make sure this auto-generated web page URL is correct.
//                // Otherwise, set the URL to null.
//                Uri.parse("http://host/path"),
//                // TODO: Make sure this auto-generated app URL is correct.
//                Uri.parse("android-app://bankkalteng.mobile.com.smsbanking.mobile.com.smsbanking.activities/http/host/path")
//        );
//        AppIndex.AppIndexApi.start(client, viewAction);
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//
//        // ATTENTION: This was auto-generated to implement the App Indexing API.
//        // See https://g.co/AppIndexing/AndroidStudio for more information.
//        Action viewAction = Action.newAction(
//                Action.TYPE_VIEW, // TODO: choose an action type.
//                "InputPin Page", // TODO: Define a title for the content shown.
//                // TODO: If you have web page content that matches this app activity's content,
//                // make sure this auto-generated web page URL is correct.
//                // Otherwise, set the URL to null.
//                Uri.parse("http://host/path"),
//                // TODO: Make sure this auto-generated app URL is correct.
//                Uri.parse("android-app://bankkalteng.mobile.com.smsbanking.mobile.com.smsbanking.activities/http/host/path")
//        );
//        AppIndex.AppIndexApi.end(client, viewAction);
//        client.disconnect();
//    }
//
//    public class MainLayout extends LinearLayout {
//
//        public MainLayout(Context context, AttributeSet attributeSet) {
//            super(context, attributeSet);
//            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            inflater.inflate(R.layout.input_pin, this);
//        }
//
//
//        @Override
//        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//            final int proposedHeight = MeasureSpec.getSize(heightMeasureSpec);
//            final int actualHeight = getHeight();
//
//            Log.d("TAG", "proposed: " + proposedHeight + ", actual: " + actualHeight);
//
//            if (actualHeight >= proposedHeight) {
//                // Keyboard is shown
//                if (mPinHiddenEditText.length() == 0)
//                    setFocusedPinBackground(mPinFirstDigitEditText);
//                else
//                    setDefaultPinBackground(mPinFirstDigitEditText);
//            }
//
//            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        }
//    }
//
//
//}
