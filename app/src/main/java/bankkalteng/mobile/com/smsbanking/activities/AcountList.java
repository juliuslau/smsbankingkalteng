package bankkalteng.mobile.com.smsbanking.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kbeanie.pinscreenlibrary.views.PinView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Random;

import bankkalteng.mobile.com.smsbanking.R;

/**
 * Created by MOBILEDEV on 10/26/2016.
 */
public class AcountList extends AppCompatActivity{

   ProgressDialog  barProgressDialog;
    Handler handler;
    RelativeLayout relativeLayout;
    Button btn_check;
    PinView pinView;
    TextView txt_saldo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.account_list);
//       myRandom(1000000.00,200000.00);
        relativeLayout = (RelativeLayout)findViewById(R.id.mainLayout);
        btn_check = (Button)findViewById(R.id.btn_check);
        txt_saldo = (TextView)findViewById(R.id.txt_saldo);
        txt_saldo.setVisibility(View.GONE);
        DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();

        formatRp.setCurrencySymbol("Rp. ");
        formatRp.setMonetaryDecimalSeparator(',');
        formatRp.setGroupingSeparator('.');
        kursIndonesia.setDecimalFormatSymbols(formatRp);
        Random rn = new Random();

        for(int i =0; i < 1; i++)
        {
            int answer = rn.nextInt(1000000) + 100000;
            String x = Integer.toString(answer);
            Double d =Double.parseDouble(x);
            txt_saldo.setText(kursIndonesia.format(d));
        }
        relativeLayout.setClickable(true);
        btn_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               launchRingDialog();


            }
        });






    }



    private void showPinUnlockScreen() {
//        Intent intent = new Intent(this, PinUnlockActivity.class);
//        startActivity(intent);



    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    public void launchRingDialog() {

        final ProgressDialog ringProgressDialog = ProgressDialog.show(AcountList.this, "Please wait ...", "Processing ...", true);

        ringProgressDialog.setCancelable(true);

        new Thread(new Runnable() {

            public void run() {

                try {

                    Thread.sleep(10000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txt_saldo.setVisibility(View.VISIBLE);
                        }
                    });

                } catch (Exception e) {

                }

                ringProgressDialog.dismiss();



            }

        }).start();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
