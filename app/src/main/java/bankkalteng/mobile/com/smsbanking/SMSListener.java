package bankkalteng.mobile.com.smsbanking;

/**
 * Created by rpj on 22/02/17.
 */

public interface SMSListener {
    public void messageReceived(String messageText);
}
