package bankkalteng.mobile.com.smsbanking.models;


import io.realm.RealmObject;

/**
 * Created by MOBILEDEV on 1/16/2017.
 */

public class AksesSetting extends RealmObject {

    private int no;
    private String ValueJalurKomunikasi;
    private String ValueTypeAkun;

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getValueJalurKomunikasi() {
        return ValueJalurKomunikasi;
    }

    public void setValueJalurKomunikasi(String valueJalurKomunikasi) {
        ValueJalurKomunikasi = valueJalurKomunikasi;
    }

    public String getValueTypeAkun() {
        return ValueTypeAkun;
    }

    public void setValueTypeAkun(String valueTypeAkun) {
        ValueTypeAkun = valueTypeAkun;
    }
}
