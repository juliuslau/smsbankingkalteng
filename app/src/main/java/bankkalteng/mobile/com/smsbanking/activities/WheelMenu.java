package bankkalteng.mobile.com.smsbanking.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import fr.rolandl.carousel.Carousel;
import fr.rolandl.carousel.CarouselAdapter;
import fr.rolandl.carousel.CarouselBaseAdapter;
import bankkalteng.mobile.com.smsbanking.R;
import bankkalteng.mobile.com.smsbanking.SMSReceiver;
import bankkalteng.mobile.com.smsbanking.adapter.WheelAdapter;
import bankkalteng.mobile.com.smsbanking.models.WheelItem;

/**
 * Created by rpj on 17/03/17.
 */

public class WheelMenu extends AppCompatActivity implements CarouselBaseAdapter.OnItemClickListener {
    public static final String PREFS_NAME = "App_Pref";
    private CarouselAdapter adapter;
    ImageView ic_info;
    private Carousel carousel;
    String valueJalurKomunikasi = "SMS";
    String valueTypeAkun = "Satu Akun";
    SharedPreferences sharedPreferences;
    private final List<WheelItem> wheelItems = new ArrayList<>();
    String userProtocol, userAccount;
    SMSReceiver smsReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wheel_layout);
        ic_info = (ImageView) findViewById(R.id.ic_info);
        carousel = (Carousel) findViewById(R.id.carousel);
        sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        userProtocol = sharedPreferences.getString("jalurProtocol", "");
        userAccount = sharedPreferences.getString("typeAkun", "");

        if (userProtocol.equalsIgnoreCase(" ") || userProtocol.isEmpty()) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("jalurProtocol", valueJalurKomunikasi);

            editor.commit();
        }
        if (userAccount.equalsIgnoreCase(" ") || userProtocol.isEmpty()) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("typeAkun", valueTypeAkun);

            editor.commit();
        }

        wheelItems.add(new WheelItem("Rekening", "ic_acc_info"));
        wheelItems.add(new WheelItem("Transfer", "ic_transfer"));
        wheelItems.add(new WheelItem("Pembelian", "ic_purchase"));
        wheelItems.add(new WheelItem("Pembayaran", "ic_payment"));
        wheelItems.add(new WheelItem("Pengaturan", "ic_setting"));
        wheelItems.add(new WheelItem("Call Center", "ic_call"));
        adapter = new WheelAdapter(this, wheelItems);
        carousel.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        carousel.setOnItemClickListener(new CarouselBaseAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(CarouselBaseAdapter<?> parent, View view, int position, long id) {
//                Toast.makeText(getApplicationContext(), "The item '" + position + "' has been clicked", Toast.LENGTH_SHORT).show();
              carousel.scrollToChild(position);

                if (position == 0){
                    startActivity(new Intent(getApplicationContext(), AccountActivity.class));
                }if(position == 1){
                    startActivity(new Intent(getApplicationContext(), TransferActivity.class));
                }if(position == 2){
                    startActivity(new Intent(getApplicationContext(), PurchaseActivity.class));
                }if(position == 3){
                    startActivity(new Intent(getApplicationContext(), PaymentActivity.class));
                }if(position == 4){
                    startActivity(new Intent(getApplicationContext(), SettingActivity.class));
                }if(position == 5){
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel: 1500526"));
                    if (ActivityCompat.checkSelfPermission(WheelMenu.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(callIntent);
                }




            }
        });

        ic_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


               myMethod();
            }
        });


    }

    public void myMethod(){
        String link1 = "<a href=\"http://www.bankkalteng.co.id\">www.bankkalteng.co.id</a>";
        String message = "<b>v 1.3.3</b><br/><br/>Mobile App SMS Banking Bank Kalteng adalah salah satu fasilitas Bank Kalteng yang memberikan kemudahan layanan bagi nasabah bank, untuk mengakses layanan perbankan melalui fitur SMS Banking / USSD.<br/>(jalur komunikasi tidak menggunakan paket data internet)<br/><br/>" + link1 +"<br/><br/>&copy; 2017 Bank Kalteng. Hak Cipta dilindungi oleh Undang-undang";

        Spanned myMessage = Html.fromHtml(message);


        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(myMessage);
        builder.setCancelable(true);
              builder.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alertDialog = builder.create();

        alertDialog.show();

        TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
        msgTxt.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void onItemClick(CarouselBaseAdapter<?> parent, View view, int position, long id) {
        Toast.makeText(getApplicationContext(), "The item '" + position + "' has been clicked", Toast.LENGTH_SHORT).show();
        carousel.scrollToChild(position);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            new SweetAlertDialog(WheelMenu.this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Keluar dari Aplikasi ?")
                    .setCancelText("Tidak")
                    .setConfirmText("Ya")
                    .showCancelButton(true)
                    .setCancelClickListener(null)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
//                            SharedPreferences.Editor editor = sharedPreferences.edit();
//                            editor.remove("jalurProtocol");
//                            editor.commit();

                            System.exit(0);
                            finish();
                        }
                    })
                    .show();


        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.remove("jalurProtocol");
//        editor.commit();

        // Stop method tracing that the activity started during onCreate()
        android.os.Debug.stopMethodTracing();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(smsReceiver);



    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(smsReceiver);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
