package bankkalteng.mobile.com.smsbanking.models;

/**
 * Created by rpj on 15/03/17.
 */

public class CarouselMenu {

    private String name;
    private int imageSource;

    public CarouselMenu(int imageSource,String name) {
        this.name = name;
        this.imageSource = imageSource;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageSource() {
        return imageSource;
    }

    public void setImageSource(int imageSource) {
        this.imageSource = imageSource;
    }
}
