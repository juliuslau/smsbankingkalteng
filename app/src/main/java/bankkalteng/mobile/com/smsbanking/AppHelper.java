package bankkalteng.mobile.com.smsbanking;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;
import com.securepreferences.SecurePreferences;

/**
 * Created by MOBILEDEV on 11/8/2016.
 */
public class AppHelper {
    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    public static Point screenSize(Context ctx)
    {
        WindowManager wm = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        if (android.os.Build.VERSION.SDK_INT >= 13) {
            display.getSize(size);
        } else {
            size.x = display.getWidth();
            size.y = display.getHeight();
        }
        return size;
    }


    public static String getPIN(Context context){
        SecurePreferences sp = new SecurePreferences(context);
        return sp.getString("user_pin", "");
    }

    public static String getProtocol(Context context){
        SecurePreferences sp = new SecurePreferences(context);
        return sp.getString("user_protocol", "");
    }

    public static String getAccountType(Context context){
        SecurePreferences sp = new SecurePreferences(context);
        return sp.getString("user_accountType", "");
    }
}
