package bankkalteng.mobile.com.smsbanking.models;

import java.io.Serializable;

/**
 * Created by rpj on 17/03/17.
 */

public class WheelItem implements Serializable {

    private static final long serialVerisonUID=  1L;
    public final String name;

    public final String image;

    public WheelItem(String name, String image) {
        this.name = name;
        this.image = image;
    }
}
