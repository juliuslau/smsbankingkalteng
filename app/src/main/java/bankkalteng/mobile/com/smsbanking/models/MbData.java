package bankkalteng.mobile.com.smsbanking.models;

import io.realm.RealmObject;

/**
 * Created by rpj on 10/02/17.
 */

public class MbData extends RealmObject {

    private String sKey;
    private String uIME;

    public String getsKey() {
        return sKey;
    }

    public void setsKey(String sKey) {
        this.sKey = sKey;
    }

    public String getuIME() {
        return uIME;
    }

    public void setuIME(String uIME) {
        this.uIME = uIME;
    }
}
