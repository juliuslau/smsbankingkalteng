package bankkalteng.mobile.com.smsbanking.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Debug;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;

import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.InputFilter;
import android.text.InputType;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.victor.loading.rotate.RotateLoading;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import pub.devrel.easypermissions.EasyPermissions;
import bankkalteng.mobile.com.smsbanking.BuildConfig;
import bankkalteng.mobile.com.smsbanking.R;
import bankkalteng.mobile.com.smsbanking.SMSListener;
import bankkalteng.mobile.com.smsbanking.SMSReceiver;

public class MainActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks{
    public static final String PREFS_NAME = "App_Pref";
    Button btnAccInfo, btnTransfer, btnPurchase, btnPayment, btnSetting, btnCall, btnUssd, btnTransparent;
    RotateLoading rotateLoading;
    Context context;
    MainActivity mainActivity;
    public String noHP, kontenSms,ussdCode;
    String userProtocol, userAccount;
    String valueJalurKomunikasi = "SMS";
    String valueTypeAkun = "Satu Akun";
    long CountSetting;
    String responseKONTEN,NO_HP ;
    SharedPreferences sharedPreferences;
    private Realm realm;
    private long chkSkey;
    private RealmConfiguration realmConfig;
    private SMSReceiver smsReceiver;
    ImageView ic_info;
    private static final int PERMISSION_REQUEST = 100;
    public static final int REQUEST_CODE_FOR_SMS = 1;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    IntentFilter intentFilter;

    SweetAlertDialog loading;
    private String[] perms;
    private TextView tv_privacy_policy;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        NO_HP = getResources().getString(R.string.NO_HP);
        intentFilter = new IntentFilter();
        intentFilter.addAction("SMS_RECEIVED_ACTION");

        perms = new String[]{Manifest.permission.SEND_SMS, Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS, Manifest.permission.CALL_PHONE};

        initUI();
        sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        userProtocol = sharedPreferences.getString("jalurProtocol", "");
        userAccount = sharedPreferences.getString("typeAkun", "");

        if (userProtocol.equalsIgnoreCase(" ") || userProtocol.isEmpty()) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("jalurProtocol", valueJalurKomunikasi);

            editor.commit();
        }
        if (userAccount.equalsIgnoreCase(" ") || userProtocol.isEmpty()) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("typeAkun", valueTypeAkun);

            editor.commit();
        }

        if(userProtocol.equals("USSD")){
            btnAccInfo.setVisibility(View.GONE);
            btnTransfer.setVisibility(View.GONE);
            btnPayment.setVisibility(View.GONE);
            btnPurchase.setVisibility(View.GONE);
            btnUssd.setVisibility(View.VISIBLE);
            getSupportActionBar().setTitle("USSD");

        }else{
            btnAccInfo.setVisibility(View.VISIBLE);
            btnTransfer.setVisibility(View.VISIBLE);
            btnPayment.setVisibility(View.VISIBLE);
            btnPurchase.setVisibility(View.VISIBLE);
            btnUssd.setVisibility(View.GONE);
        }


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        getPermissionToReadSMS();

        SMSReceiver.bindListener(new SMSListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void messageReceived(String messageText) {

                final Matcher matcher = Pattern.compile("disertai digit").matcher(messageText);
                if(matcher.find()){
                    AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                    alert.setMessage(messageText); //Message here

                    LinearLayout layout = new LinearLayout(MainActivity.this);
                    layout.setOrientation(LinearLayout.VERTICAL);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(20, 0, 30, 0);

                    final EditText input = new EditText(MainActivity.this);
                    layout.addView(input,params);
                    input.setInputType(InputType.TYPE_CLASS_TEXT);


                    input.setMaxLines(5);
                    input.setMaxWidth(50);
                    input.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5) {

                    }});


                    alert.setView(layout);


                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            responseKONTEN = input.getText().toString();

                            if (input.getText().length()==0){
                                input.setError("masukan index rekening");
                                return;
                            }else {

                                sendSMS(NO_HP,responseKONTEN);
                                return;
                            }


                        } // End of onClick(DialogInterface dialog, int whichButton)
                    }); //End of alert.setPositiveButton


                    alert.setCancelable(false);
                    alert.show();

                }else{

                    new SweetAlertDialog(MainActivity.this, SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText("")
                            .setContentText(messageText)
                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                }

            }
        });
//        if (checkAndRequestPermissions()) {
//            // carry on the normal flow, as the case of  permissions  granted.
//        }




//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (checkSelfPermission(Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
//                if (shouldShowRequestPermissionRationale(Manifest.permission.SEND_SMS)) {
//                    Snackbar.make(findViewById(R.id.relativeLayout), "You need to grant SEND SMS permission to send sms",
//                            Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                                requestPermissions(new String[]{Manifest.permission.SEND_SMS}, PERMISSION_REQUEST);
//                            }
//                        }
//                    }).show();
//                } else {
//                    requestPermissions(new String
//                            []{Manifest.permission.SEND_SMS}, PERMISSION_REQUEST);
//                }
//
//
//            }
//            Toast.makeText(this.getApplicationContext(),"6 >", Toast.LENGTH_LONG).show();
//        } else {
//
//        }


//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            if (checkSelfPermission(Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
//                if (shouldShowRequestPermissionRationale(Manifest.permission.RECEIVE_SMS)) {
//                    Snackbar.make(findViewById(R.id.relativeLayout), "You need to grant RECEIVE SMS permission to receive sms",
//                            Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                                requestPermissions(new String[]{Manifest.permission.RECEIVE_SMS}, REQUEST_CODE_FOR_SMS);
//                            }
//                        }
//                    }).show();
//                } else {
//                    requestPermissions(new String[]{Manifest.permission.RECEIVE_SMS}, REQUEST_CODE_FOR_SMS);
//                }
//            }  else {
//
//            }
//        } else {
//
//        }

//showPINSetupScreen();
//

//        realmConfig = new RealmConfiguration.Builder(getApplicationContext()).build();
////        Realm.deleteRealm(realmConfig);
//        realm = Realm.getInstance(realmConfig);

//        realmConfig = new RealmConfiguration.Builder(getApplication()).build();
//        realm = Realm.getInstance(realmConfig);
//        chkSkey = realm.where(MbData.class).count();
//
//        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
//        String x = telephonyManager.getDeviceId();
//
//        Toast.makeText(
//                this.getApplicationContext(),"JDEVICE ID = " +x, Toast.LENGTH_LONG).show();
//
//        Toast.makeText(
//                this.getApplicationContext(),"S-KEY = " +chkSkey, Toast.LENGTH_LONG).show();


        tv_privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                openPrivacyPolicy();
            }
        });

        btnUssd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ussdCode = "*" + "141" + "*" + "125" + Uri.encode("#");
                if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(MainActivity.this,"You Should Allow Call Permission on Settings!",Toast.LENGTH_LONG).show();
                    return;
                }
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));


            }

        });


        btnAccInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AccountActivity.class);
                startActivity(intent);
            }
        });


        btnTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, TransferActivity.class);
                startActivity(intent);
            }

        });

        btnPurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PurchaseActivity.class);
                startActivity(intent);
            }

        });

        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PaymentActivity.class);
                startActivity(intent);
            }

        });

        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(intent);

            }

        });

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel: 1500526"));
                if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    Toast.makeText(MainActivity.this,"You should allow permission for call phone on Settings to use this feature",Toast.LENGTH_LONG).show();
                    return;
                }
                startActivity(callIntent);

            }

        });

        ic_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

                alert.setMessage("v "+BuildConfig.VERSION_NAME +"\n\nMobile App SMS Banking Bank Kalteng adalah salah satu fasilitas Bank Kalteng yang memberikan kemudahan layanan bagi nasabah bank, untuk mengakses layanan perbankan melalui fitur SMS Banking / USSD.\n\n (jalur komunikasi tidak menggunakan paket data internet)\n\n \u00a9 2019 Bank Kalteng. Hak Cipta dilindungi oleh Undang-undang");
                alert.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        });




    }

    private void openPrivacyPolicy() {

        Intent intent = new Intent(this, ActivityFaqWeb.class);
        startActivity(intent);
    }

    private static final int READ_SMS_PERMISSIONS_REQUEST = 1;

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void getPermissionToReadSMS() {
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)
//                != PackageManager.PERMISSION_GRANTED) {
//            if (shouldShowRequestPermissionRationale(
//                    Manifest.permission.READ_SMS)) {
//                Toast.makeText(this, "Please allow permission!", Toast.LENGTH_SHORT).show();
//            }
//            requestPermissions(new String[]{Manifest.permission.READ_SMS},
//                    READ_SMS_PERMISSIONS_REQUEST);
//        }

        if (EasyPermissions.hasPermissions(this, perms)) {

        }
        else {
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_phone_state), READ_SMS_PERMISSIONS_REQUEST, perms);
            return;
        }
    }

    private void initUI (){
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        tv_privacy_policy = findViewById(R.id.tv_privacy_policy);
        ic_info = (ImageView) findViewById(R.id.ic_info);
        btnAccInfo = (Button) findViewById(R.id.menu_acc_info);
        btnPurchase = (Button) findViewById(R.id.menu_purchase);
        btnPayment = (Button) findViewById(R.id.menu_payment);
        btnTransfer = (Button) findViewById(R.id.menu_transfer);
        btnSetting = (Button) findViewById(R.id.menu_setting);
        btnCall = (Button) findViewById(R.id.menu_info);
        btnUssd = (Button) findViewById(R.id.menu_ussd);
        btnUssd.setVisibility(View.GONE);
//        btnTransparent.setClickable(false);

//        btnCall= (Button)findViewById(R.id.menu_info);
//        btnCall.setClickable(false);
//        btnCall.setVisibility(View.GONE);
        Animation anim_left = AnimationUtils.loadAnimation(getApplication(), R.anim.push_left);
        Animation anim_right = AnimationUtils.loadAnimation(getApplication(), R.anim.push_right);
//        btnAccInfo.startAnimation(anim_left);
        btnTransfer.startAnimation(anim_right);
        btnPurchase.startAnimation(anim_right);
        btnPayment.startAnimation(anim_left);
        btnSetting.startAnimation(anim_left);
        btnCall.startAnimation(anim_right);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
//
//        // Make sure it's our original READ_CONTACTS request
//        if (requestCode == READ_SMS_PERMISSIONS_REQUEST) {
//            if (grantResults.length == 1 &&
//                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//            } else {
//                Toast.makeText(this, "Read SMS permission denied", Toast.LENGTH_SHORT).show();
//            }
//
//        } else {
//            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        }
    }


    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        switch (requestCode) {
            case REQUEST_CODE_FOR_SMS:
//                Toast.makeText(MainActivity.this, "Read SMS permission granted", Toast.LENGTH_SHORT).show();
                break;

        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        switch (requestCode) {
            case REQUEST_CODE_FOR_SMS:
//                Toast.makeText(MainActivity.this, getString(R.string.on_permission_cancelled_read_phone_state), Toast.LENGTH_SHORT).show();
                finish();
                break;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkPermission(){
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>=android.os.Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.SEND_SMS)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("Send SMS permission is necessary !");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity)context, new String[]{Manifest.permission.SEND_SMS}, REQUEST_CODE_FOR_SMS);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    ActivityCompat.requestPermissions((Activity)context, new String[]{Manifest.permission.SEND_SMS}, REQUEST_CODE_FOR_SMS);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }

    }


    private boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS);
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.SEND_SMS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    public void showPINSetupScreen() {
//        Intent intent = new Intent(MainActivity.this, PinSetupActivity.class);
//        startActivity(intent);
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();

        Debug.stopMethodTracing();

        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        super.finish();



    }

    @Override
    protected void onPause() {
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        super.onPause();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Keluar dari Aplikasi ?")
                    .setCancelText("Tidak")
                    .setConfirmText("Ya")
                    .showCancelButton(true)
                    .setCancelClickListener(null)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            android.os.Process.killProcess(android.os.Process.myPid());
                            System.exit(1);
                            finish();

                        }
                    })
                    .show();


        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    protected void onResume() {

        registerReceiver(sendBroadcastReceiver,intentFilter);
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        registerReceiver(sendBroadcastReceiver,intentFilter);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        if(client != null){
            client.connect();
            AppIndex.AppIndexApi.start(client, getIndexApiAction());
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.

        if(client != null){

            AppIndex.AppIndexApi.end(client, getIndexApiAction());
            client.disconnect();
        }
    }

    private void sendSMS(String phoneNumber, String message) {

        loading = new SweetAlertDialog(MainActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setTitleText("Memproses...");

        loading.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                text.setGravity(Gravity.CENTER);
            }
        });
        loading.show();


        new CountDownTimer(30000, 1000) { //40000 milli seconds is total time, 1000 milli seconds is time interval

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                loading.dismiss();
            }
        }.start();




        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
                SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

        registerReceiver(deliverBroadcastReceiver, new IntentFilter(DELIVERED));
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);



    }

    BroadcastReceiver sendBroadcastReceiver = new SentReceiver();
    BroadcastReceiver deliverBroadcastReceiver = new DeliverReceiver();


    class DeliverReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:

                    loading.dismiss();
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(getBaseContext(), "Gagal Terkirimm",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }


    class SentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "Mengirim", Toast.LENGTH_SHORT)
                            .show();
                    loading.dismiss();
                    new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Perhatian !")
                            .setContentText("Mohon menunggu sampai anda mendapat informasi balasan dari kami !")

                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Toast.makeText(getBaseContext(), "Generic failure",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Toast.makeText(getBaseContext(), "No service",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT)
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Toast.makeText(getBaseContext(), "Radio off",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }
}



