package bankkalteng.mobile.com.smsbanking.activities;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import bankkalteng.mobile.com.smsbanking.R;

/**
 * Created by MOBILEDEV on 11/9/2016.
 */
public class PurchaseGeneralActivity extends AppCompatActivity {


    private ListActivity listActivity;
    public String purchaseCategory;
    public String[] listData;
    public void onCreate(Bundle savedInstanceState) {

        Intent intent = getIntent();
        purchaseCategory= intent.getStringExtra("purchaseCategory");
        super.onCreate(savedInstanceState);
        listActivity = new ListActivity();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.purchase_layout);
        // storing string resources into Array
        setTitle(purchaseCategory);
        try {
            if(purchaseCategory.equals("Pulsa")){
                listData = getResources().getStringArray(R.array.purchase_list_phone_voucher);

            }if(purchaseCategory.equals("Listrik")){
                listData = getResources().getStringArray(R.array.purchase_list_electricity);
            }if(purchaseCategory.equals("Tiket")){
                listData = getResources().getStringArray(R.array.purchase_list_ticket);
            }if(purchaseCategory.equals("Lainya")){
                listData = getResources().getStringArray(R.array.purchase_list_others);
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        // Binding resources Array to ListAdapter


        ArrayAdapter<String> adapter =new ArrayAdapter<String>(this, R.layout.list_item_arrow, R.id.label, listData);
        ListView lv = (ListView)findViewById(R.id.lv);
        lv.setAdapter(adapter);



        // listening to single list item on click
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

//                AlertDialog.Builder builder = new AlertDialog.Builder(PurchaseGeneralActivity.this);
//                builder.setMessage("We Apologize, it will be available soon")
//                        .setTitle("Purchase")
//                        .setPositiveButton(android.R.string.ok, null);
//                AlertDialog dialog = builder.create();
//                dialog.show();

                String data=(String)parent.getItemAtPosition(position);
                Toast.makeText(getApplication(), data,
                        Toast.LENGTH_LONG).show();
            }
        });
    }
}
