package bankkalteng.mobile.com.smsbanking.activities;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import bankkalteng.mobile.com.smsbanking.R;

/**
 * Created by MOBILEDEV on 12/13/2016.
 */
public class VoucherHPPurchase extends AppCompatActivity {


    AutoCompleteTextView sourceAccount,denomList;
    EditText kodemerchant, kodebayar;
    Button btnSubmit;
    private ProgressDialog pDialog;
    String INDEX_REK1, BILLER_NO, KONTEN_SMS,DENOM, NO_HP;

    String[] sourceAcct={"1","2","3","4","5"};
    String[] denom={"20000","25000","50000","100000","200000","500000","1000000"};
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.voucherhp_purchase);
        KONTEN_SMS = getResources().getString(R.string.SMS_PEMBELIAN_PULSA);
        NO_HP =getResources().getString(R.string.NO_HP);
        sourceAccount=(AutoCompleteTextView)findViewById(R.id.autoCompleteTextView1);
        denomList=(AutoCompleteTextView)findViewById(R.id.autoCompleteTextView2);
        kodemerchant=(EditText) findViewById(R.id.txt_merchant);
//        kodebayar =(EditText)findViewById(R.id.txt_kodebayar);

        btnSubmit = (Button)findViewById(R.id.btnSubmit);

        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,sourceAcct);
        sourceAccount.setAdapter(adapter);
        sourceAccount.setThreshold(1);

        ArrayAdapter adapters = new ArrayAdapter(this,android.R.layout.simple_list_item_1,denom);
        denomList.setAdapter(adapters);
        denomList.setThreshold(1);






        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (sourceAccount.getText().toString().equals("")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(VoucherHPPurchase.this);
                        builder.setMessage("Mohon pilih index rekening !")
                                .setTitle("Pembelian Voucher HP")
                                .setPositiveButton(android.R.string.ok, null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        return;
                    }
                    if (kodemerchant.getText().toString().equals("")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(VoucherHPPurchase.this);
                        builder.setMessage("Mohon masukan no hp !")
                                .setTitle("Pembelian Voucher HP")
                                .setPositiveButton(android.R.string.ok, null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        return;
                    }if (sourceAccount.getText().toString().equals("")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(VoucherHPPurchase.this);
                        builder.setMessage("Mohon pilih nominal voucher !")
                                .setTitle("Pembelian Voucher HP")
                                .setPositiveButton(android.R.string.ok, null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        return;
                    }else {
                        process();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }



            }

        });
    }


    public void process() {


        INDEX_REK1 = sourceAccount.getText().toString();
        BILLER_NO = kodemerchant.getText().toString();
        DENOM = kodebayar.getText().toString();
        String KONTEN = KONTEN_SMS + " " + INDEX_REK1 + " " + BILLER_NO + " " + DENOM;

        AlertDialog.Builder builder = new AlertDialog.Builder(VoucherHPPurchase.this);
        builder.setMessage(KONTEN)
                .setTitle("Pembelian Voucher HP")
                .setPositiveButton(android.R.string.ok, null);
        AlertDialog dialogs = builder.create();
        dialogs.show();


        try {
            pDialog = ProgressDialog.show(VoucherHPPurchase
                            .this, "SMS Banking",
                    "sedang memproses ...", true);
            String SENT = "sent";
            String DELIVERED = "delivered";

            Intent sentIntent = new Intent(SENT);
     /*Create Pending Intents*/
            PendingIntent sentPI = PendingIntent.getBroadcast(
                    getApplicationContext(), 0, sentIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            Intent deliveryIntent = new Intent(DELIVERED);

            PendingIntent deliverPI = PendingIntent.getBroadcast(
                    getApplicationContext(), 0, deliveryIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
     /* Register for SMS send action */
            registerReceiver(new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {
                    String result = "";

                    switch (getResultCode()) {

                        case Activity.RESULT_OK:
                            result = "Transmission successful";
                            break;
                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                            result = "Transmission failed";
                            break;
                        case SmsManager.RESULT_ERROR_RADIO_OFF:
                            result = "Radio off";
                            break;
                        case SmsManager.RESULT_ERROR_NULL_PDU:
                            result = "No PDU defined";
                            break;
                        case SmsManager.RESULT_ERROR_NO_SERVICE:
                            result = "No service";
                            break;
                    }

                    Toast.makeText(getApplicationContext(), result,
                            Toast.LENGTH_LONG).show();
                    pDialog.dismiss();
                }

            }, new IntentFilter(SENT));
     /* Register for Delivery event */
            registerReceiver(new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {
//                            Toast.makeText(getApplicationContext(), "Deliverd",
//                                    Toast.LENGTH_LONG).show();

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                            VoucherHPPurchase.this);

                    // set title
                    alertDialogBuilder.setTitle("SMS Banking");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Silahkan cek pesan masuk anda")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    openInbox();
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }

            }, new IntentFilter(DELIVERED));

      /*Send SMS*/
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(NO_HP, null, KONTEN, sentPI,
                    deliverPI);
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(),
                    ex.getMessage().toString(), Toast.LENGTH_LONG)
                    .show();
            ex.printStackTrace();


        }




    }

    public void openInbox() {
        String application_name = "com.android.mms";
        try {
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.LAUNCHER");

            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            List<ResolveInfo> resolveinfo_list = this.getPackageManager()
                    .queryIntentActivities(intent, 0);

            for (ResolveInfo info : resolveinfo_list) {
                if (info.activityInfo.packageName
                        .equalsIgnoreCase(application_name)) {
                    launchComponent(info.activityInfo.packageName,
                            info.activityInfo.name);
                    break;
                }
            }
        } catch (ActivityNotFoundException e) {
            Toast.makeText(
                    this.getApplicationContext(),
                    "There was a problem loading the application: "
                            + application_name, Toast.LENGTH_SHORT).show();
        }
    }

    private void launchComponent(String packageName, String name) {
        Intent launch_intent = new Intent("android.intent.action.MAIN");
        launch_intent.addCategory("android.intent.category.LAUNCHER");
        launch_intent.setComponent(new ComponentName(packageName, name));
        launch_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(launch_intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id != null) {
//            return true;
//        }
        return super.onOptionsItemSelected(item);
    }

}
