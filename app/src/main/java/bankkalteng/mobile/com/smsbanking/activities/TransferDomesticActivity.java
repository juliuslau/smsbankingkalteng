package bankkalteng.mobile.com.smsbanking.activities;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.text.method.KeyListener;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import bankkalteng.mobile.com.smsbanking.R;
import bankkalteng.mobile.com.smsbanking.SMSListener;
import bankkalteng.mobile.com.smsbanking.SMSReceiver;

/**
 * Created by MOBILEDEV on 10/27/2016.
 */
public class TransferDomesticActivity extends AppCompatActivity {
    public static final String PREFS_NAME = "App_Pref";
    AutoCompleteTextView sourceAccount, listBanks;
    EditText nominal,berita,benef,benefAccount;
    Button btnSubmit, btnIndex;
    String bcd, getBcd;
    SharedPreferences sharedPreferences;
    private ProgressDialog pDialog;
    String responseKONTEN,INDEX_REK1, INDEX_REK2,BERITA, KONTEN_SMS,NOMINAL, NO_HP,FORMAT_SMS, NO_REK_TUJUAN,KODE_BANK,MESSAGE,defaultAccountType,defaultProtocol,ussdCode;;
    String defaultRekening = "1";
    String[] sourceAcct={"1","2","3","4","5"};
    String[] BenefAcct={"1","2","3","4","5"};
    RelativeLayout lyt_1, lyt_2, lyt_3;
    TextView lbl_index_rek;
    List<String> bankName, bankCode;

    IntentFilter intentFilter;
    SweetAlertDialog loading;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.domestic_transfer_layout);
        sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        intentFilter = new IntentFilter();
        intentFilter.addAction("SMS_RECEIVED_ACTION");
        defaultAccountType= sharedPreferences.getString("typeAkun", "");
        defaultProtocol = sharedPreferences.getString("jalurProtocol", "");
        FORMAT_SMS = getResources().getString(R.string.SMS_TRANSFER_DOMESTIC);
        NO_HP =getResources().getString(R.string.NO_HP);
        sourceAccount=(AutoCompleteTextView)findViewById(R.id.autoCompleteTextView1);
        benefAccount=(EditText)findViewById(R.id.txt_benef);
        nominal =(EditText)findViewById(R.id.txt_nominal);
        benef =(EditText)findViewById(R.id.txt_benef);
        berita =(EditText)findViewById(R.id.txt_berita);
//        bcd =(EditText)findViewById(R.id.txt_bankcode);
        listBanks=(AutoCompleteTextView)findViewById(R.id.autoCompleteBankCode);
        btnSubmit = (Button)findViewById(R.id.btnSubmit);
        btnIndex = (Button)findViewById(R.id.btnIndex);
        bankName = Arrays.asList(getResources().getStringArray(R.array.bank_name));



//        Toast.makeText(getBaseContext(),bankName.toString(),Toast.LENGTH_LONG).show();

        final ArrayList<BankList> list = new ArrayList<BankList>();
        ArrayAdapter Badapter = new
                ArrayAdapter(this,android.R.layout.simple_list_item_1,bankName);


        listBanks.setAdapter(Badapter);
       listBanks.setThreshold(1);
//
//        lyt_1 = (RelativeLayout)findViewById(R.id.lyt_1);
//        lbl_index_rek = (TextView)findViewById(R.id.lbl_index_rek);

        defaultAccountType= sharedPreferences.getString("typeAkun", "");
        defaultProtocol = sharedPreferences.getString("jalurProtocol", "");
        if(defaultAccountType.equalsIgnoreCase("Satu Akun")){
            sourceAccount.setFilters(new InputFilter[]{new InputFilter.LengthFilter(35) {

            }});
            sourceAccount.setText("index rekening 1");
            sourceAccount.setTextSize(14);
            sourceAccount.setClickable(false);
            sourceAccount.setCursorVisible(false);
            sourceAccount.setEnabled(false);
        }else{
            ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,sourceAcct);
            sourceAccount.setAdapter(adapter);
            sourceAccount.setThreshold(1);
            sourceAccount.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1) {

            }});
        }


        final InputMethodManager imm = (InputMethodManager)getApplication().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(nominal.getWindowToken(), 0);
        sourceAccount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                imm.showSoftInputFromInputMethod(nominal.getWindowToken(), 0);
            }
        });

        benefAccount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                imm.showSoftInputFromInputMethod(nominal.getWindowToken(), 0);
            }
        });

        nominal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                imm.showSoftInputFromInputMethod(nominal.getWindowToken(), 0);
            }
        });

        sourceAccount.setInputType(InputType.TYPE_CLASS_PHONE);
        benefAccount.setInputType(InputType.TYPE_CLASS_PHONE);
        nominal.setInputType(InputType.TYPE_CLASS_PHONE);
        KeyListener keyListener = DigitsKeyListener.getInstance("1234567890");
        sourceAccount.setKeyListener(keyListener);
        benefAccount.setKeyListener(keyListener);
        nominal.setKeyListener(keyListener);
//        bcd.setKeyListener(keyListener);


        SMSReceiver.bindListener(new SMSListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void messageReceived(String messageText) {

                final Matcher matcher = Pattern.compile("disertai digit").matcher(messageText);

                if(matcher.find()){
                    AlertDialog.Builder alert = new AlertDialog.Builder(TransferDomesticActivity.this);
                    alert.setMessage(messageText); //Message here

                    LinearLayout layout = new LinearLayout(TransferDomesticActivity.this);
                    layout.setOrientation(LinearLayout.VERTICAL);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(20, 0, 30, 0);

                    final EditText input = new EditText(TransferDomesticActivity.this);
                    layout.addView(input,params);
                    input.setInputType(InputType.TYPE_CLASS_TEXT);


                    input.setMaxLines(5);
                    input.setMaxWidth(50);
                    input.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5) {

                    }});


                    alert.setView(layout);


                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            responseKONTEN = input.getText().toString();

                            if (input.getText().length()==0){
                                input.setError("masukan index rekening");
                                return;
                            }else {

                                sendSMS(NO_HP,responseKONTEN);
                                return;
                            }


                        } // End of onClick(DialogInterface dialog, int whichButton)
                    }); //End of alert.setPositiveButton


                    alert.setCancelable(false);
                    alert.show();
                    return;

                }else{
                    new SweetAlertDialog(TransferDomesticActivity.this, SweetAlertDialog.NORMAL_TYPE)
                             .setTitleText("")
                            .setContentText(messageText)
                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    return;
                }

            }
        });

        btnIndex.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                KONTEN_SMS = getResources().getString(R.string.SMS_INDEX_REKENING);
                sendSMS(NO_HP,KONTEN_SMS);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if(defaultAccountType.equalsIgnoreCase("Satu Akun")) {
                        if (defaultProtocol.equalsIgnoreCase("SMS")) {

                            if (listBanks.getText().toString().equals("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(TransferDomesticActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan kode bank tujuan !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                listBanks.setError("");
                                return;
                            }
                            if (benef.getText().toString().equals("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(TransferDomesticActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan rekening tujuan !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                benef.setError("");
                                return;
                            }if(benef.getText().toString().length() < 10 || benef.getText().toString().length() > 16){
                                SweetAlertDialog loading = new SweetAlertDialog(TransferDomesticActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan rekening tujuan dengan benar !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                benef.setError("");
                                return;
                            }
                            if (nominal.getText().toString().equals("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(TransferDomesticActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan jumlah transfer !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                nominal.setError("");
                                return;
                            }
                            else{
                                String regBcd = listBanks.getText().toString();
//                                getBcd = regBcd.substring(regBcd.indexOf("(")+1,regBcd.indexOf(")"));

                                INDEX_REK2 = benefAccount.getText().toString();
                                KODE_BANK = regBcd.substring(regBcd.indexOf("(")+1,regBcd.indexOf(")"));
                                NOMINAL = nominal.getText().toString();
                                BERITA = berita.getText().toString();
                                if(BERITA.equalsIgnoreCase("")){
                                    KONTEN_SMS = FORMAT_SMS + " " + defaultRekening + " " + KODE_BANK + " " + INDEX_REK2 + " " +NOMINAL+"#";
                                }else{
                                    KONTEN_SMS = FORMAT_SMS + " " + defaultRekening + " " + KODE_BANK + " " + INDEX_REK2 + " " +NOMINAL+"#"+BERITA;
                                }


                                sendSMS(NO_HP,KONTEN_SMS);

                                Toast.makeText(TransferDomesticActivity.this, "NO HP : " + NO_HP + "/n" + "KONTEN SMS : " + KONTEN_SMS ,Toast.LENGTH_SHORT).show();
                            }


                        }else{
                            ussdCode = "*" + "141" + "*" + "125" + Uri.encode("#");
                            if (ActivityCompat.checkSelfPermission(TransferDomesticActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            if (ActivityCompat.checkSelfPermission(TransferDomesticActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
                        }
                    }else{
                        if (defaultProtocol.equalsIgnoreCase("SMS")) {

                            if (sourceAccount.getText().toString().equals("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(TransferDomesticActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan index rekening !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                sourceAccount.setError("");
                                return;
                            } if (listBanks.getText().toString().equals("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(TransferDomesticActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan kode bank tujuan !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                listBanks.setError("");
                                return;
                            }
                            if (benef.getText().toString().equals("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(TransferDomesticActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan rekening tujuan !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                benef.setError("");
                                return;
                            }
                            if (nominal.getText().toString().equals("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(TransferDomesticActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan jumlah transfer !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                nominal.setError("");
                                return;
                            }
                            else{
                                INDEX_REK1 = sourceAccount.getText().toString();
                                INDEX_REK2 = benefAccount.getText().toString();
                                String regBcd = listBanks.getText().toString();
                                KODE_BANK = regBcd.substring(regBcd.indexOf("(")+1,regBcd.indexOf(")"));

                                NOMINAL = nominal.getText().toString();
                                BERITA = berita.getText().toString();
                                if(BERITA.equalsIgnoreCase("")){
                                    KONTEN_SMS = FORMAT_SMS + " " + INDEX_REK1 + " " + KODE_BANK + " " + INDEX_REK2 + " " +NOMINAL+"#";
                                }else{
                                    KONTEN_SMS = FORMAT_SMS + " " + INDEX_REK1 + " " + KODE_BANK + " " + INDEX_REK2 + " " +NOMINAL+"#"+BERITA;
                                }


                                sendSMS(NO_HP,KONTEN_SMS);
                                Toast.makeText(TransferDomesticActivity.this, "NO HP : " + NO_HP + "/n" + "KONTEN SMS : " + KONTEN_SMS ,Toast.LENGTH_SHORT).show();

                            }


                        }else{
                            ussdCode = "*" + "141" + "*" + "3" + "*" + "1" + "*" + INDEX_REK1 + "*" + KODE_BANK + "*" + INDEX_REK2 + "*" + NOMINAL + Uri.encode("#");
                            if (ActivityCompat.checkSelfPermission(TransferDomesticActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            if (ActivityCompat.checkSelfPermission(TransferDomesticActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }



            }

        });

        SMSReceiver.bindListener(new SMSListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void messageReceived(String messageText) {

                final Matcher matcher = Pattern.compile("disertai digit").matcher(messageText);
                final Matcher matcherIndex = Pattern.compile("Index Rekening").matcher(messageText);
                if(matcher.find()){
                    AlertDialog.Builder alert = new AlertDialog.Builder(TransferDomesticActivity.this);
                    alert.setMessage(messageText); //Message here

                    LinearLayout layout = new LinearLayout(TransferDomesticActivity.this);
                    layout.setOrientation(LinearLayout.VERTICAL);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(20, 0, 30, 0);

                    final EditText input = new EditText(TransferDomesticActivity.this);
                    layout.addView(input,params);
                    input.setInputType(InputType.TYPE_CLASS_TEXT);


                    input.setMaxLines(5);
                    input.setMaxWidth(50);
                    input.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5) {

                    }});


                    alert.setView(layout);


                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            responseKONTEN = input.getText().toString();

                            if (input.getText().length()==0){
                                input.setError("masukan index rekening");
                                return;
                            }else {

                                sendSMS(NO_HP,responseKONTEN);
                                return;
                            }


                        } // End of onClick(DialogInterface dialog, int whichButton)
                    }); //End of alert.setPositiveButton


                    alert.setCancelable(true);
                    alert.show();
                    return;

                }else{
                    new SweetAlertDialog(TransferDomesticActivity.this, SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText("")
                            .setContentText(messageText)
                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    return;
                }

            }
        });
    }





    public void ClearField(){
        if(defaultAccountType.equalsIgnoreCase("Satu Akun")){
            sourceAccount.setFilters(new InputFilter[]{new InputFilter.LengthFilter(35) {

            }});
            sourceAccount.setText("index rekening 1");
            sourceAccount.setTextSize(12);
            sourceAccount.setClickable(false);
            sourceAccount.setCursorVisible(false);
            sourceAccount.setEnabled(false);
        }else{
            sourceAccount.getText().clear();
        }
        benefAccount.getText().clear();
        nominal.getText().clear();
        listBanks.getText().clear();
        berita.getText().clear();
    }

    public void openInbox() {
        String application_name = "com.android.mms";
        try {
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.LAUNCHER");

            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            List<ResolveInfo> resolveinfo_list = this.getPackageManager()
                    .queryIntentActivities(intent, 0);

            for (ResolveInfo info : resolveinfo_list) {
                if (info.activityInfo.packageName
                        .equalsIgnoreCase(application_name)) {
                    launchComponent(info.activityInfo.packageName,
                            info.activityInfo.name);
                    break;
                }
            }
        } catch (ActivityNotFoundException e) {
            Toast.makeText(
                    this.getApplicationContext(),
                    "There was a problem loading the application: "
                            + application_name, Toast.LENGTH_SHORT).show();
        }
    }

    private void launchComponent(String packageName, String name) {
        Intent launch_intent = new Intent("android.intent.action.MAIN");
        launch_intent.addCategory("android.intent.category.LAUNCHER");
        launch_intent.setComponent(new ComponentName(packageName, name));
        launch_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(launch_intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id != null) {
//            return true;
//        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        registerReceiver(sendBroadcastReceiver,intentFilter);
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        super.onDestroy();
    }


    private void sendSMS(String phoneNumber, String message) {

        loading = new SweetAlertDialog(TransferDomesticActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setTitleText("Memproses...");

        loading.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                text.setGravity(Gravity.CENTER);
            }
        });
        loading.show();


        new CountDownTimer(30000, 1000) { //40000 milli seconds is total time, 1000 milli seconds is time interval

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                loading.dismiss();
            }
        }.start();




        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
                SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

        registerReceiver(deliverBroadcastReceiver, new IntentFilter(DELIVERED));
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
        ClearField();


    }

    BroadcastReceiver sendBroadcastReceiver = new SentReceiver();
    BroadcastReceiver deliverBroadcastReceiver = new DeliverReceiver();


    class DeliverReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    loading.dismiss();
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(getBaseContext(), "Gagal Mengirim",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }


    class SentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "MENGIRIM", Toast.LENGTH_SHORT)
                            .show();
                    loading.dismiss();
                    new SweetAlertDialog(TransferDomesticActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Perhatian !")
                            .setContentText("Mohon menunggu sampai anda mendapat informasi balasan dari kami !")

                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Toast.makeText(getBaseContext(), "Generic failure",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Toast.makeText(getBaseContext(), "No service",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT)
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Toast.makeText(getBaseContext(), "Radio off",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }

    public class BankList{
        private String bankName;
        private String bankCode;

        public BankList(String bankName, String bankCode) {
            this.bankName = bankName;
            this.bankCode = bankCode;
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }
}
