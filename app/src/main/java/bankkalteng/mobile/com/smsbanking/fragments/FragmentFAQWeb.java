package bankkalteng.mobile.com.smsbanking.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ClientCertRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import bankkalteng.mobile.com.smsbanking.BuildConfig;
import bankkalteng.mobile.com.smsbanking.R;
import bankkalteng.mobile.com.smsbanking.network.InetHandler;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;


public class FragmentFAQWeb extends Fragment {

    public static int TYPE_FIRST_TIME_SHOW = 1;
    public static int TYPE_ONLY_SHOW = 2;



    Unbinder unbinder;

    ViewGroup container;
    private Boolean isDisconnected;
    private View v;

    @BindView(R.id.loading_bar_web)
    ProgressBar pb_webview;
    @BindView(R.id.webview)
    WebView webView;

    @BindView(R.id.lyt_content)
    RelativeLayout lyt_content;

    @BindView(R.id.rootLyt)
    RelativeLayout rootLyt;

    public  OnTapItemListener listener;

    private int type;


    public interface OnTapItemListener{
        void onSubmit(DialogFragment dialog);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        initData();


    }

    private void initData() {

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_faq_full_screen, container, false);
        unbinder = ButterKnife.bind(this,v);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        if(InetHandler.isNetworkAvailable(getActivity())){


            String URL_TERMS = BuildConfig.URL_PRIVACY_POLICY;

            loadUrl(URL_TERMS);
        }else{
            Toast.makeText(getActivity(),getString(R.string.network_connection_failure_toast),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unbinder.unbind();
//        destroyWebView();

    }

    private void showView(){

        lyt_content.setVisibility(View.VISIBLE);
    }



    @SuppressWarnings("deprecation")
    private void loadUrl(String url) {
//        webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        // allow web to be wide
//        webSettings.setUseWideViewPort(true);
//        webSettings.setLoadWithOverviewMode(true);
        // allow web to be pinch resize
//        webSettings.setSupportZoom(true);
//        webSettings.setBuiltInZoomControls(true);
//        webSettings.setDisplayZoomControls(false);

        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        if (android.os.Build.VERSION.SDK_INT<=11) {
            webSettings.setAppCacheMaxSize(1024 * 1024 * 8);
        }


        //final Activity activity = this;
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                //setSupportProgress(progress * 100);
//               getProgressSpinner().setVisibility(View.VISIBLE);
                //activity.setProgress(progress * 100);
        /*if(progress == 100)
            getProgressSpinner().setVisibility(View.GONE);*/

                //pb_webview.setVisibility(View.VISIBLE);
                pb_webview.setIndeterminate(true);
                if (progress == 100) {

                    showView();
                    pb_webview.setVisibility(View.GONE);
                }
            }
        });
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                webView.loadUrl("javascript:(function () {document.getElementsByTagName('body')[0].style.marginBottom = '0'})()");
                super.onPageFinished(view, url);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Timber.d("isi url tombol-tombolnya:" + url);
                //view.loadUrl(url);
//                if (url.contains("isAgree=true")) {
//
//                    setPreference(true);
//                }else{
//                    webView.loadUrl(url);
//                }
                return true;
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Timber.d("isi failing url tombol-tombolnya:" + failingUrl);
                isDisconnected = true;
            }


            @Override
            public void onReceivedClientCertRequest(WebView view, ClientCertRequest request) {
                super.onReceivedClientCertRequest(view, request);
            }
        });


        webView.loadUrl(url);
    }

    private void setPreference(boolean isRead){
        if(isRead){
//            PreferenceManager.getInstance().setBool(DefineValue.IS_AGREE_TNC,isRead);
        }
    }


    private void showDialogFailure(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setMessage(getString(R.string.network_connection_failure_toast))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    @OnClick(R.id.btnBack)
    void onBack(){

        result();
    }



    private void result() {

        getActivity().finish();
    }

    public void destroyWebView() {

        // Make sure you remove the WebView from its parent view before doing anything.
        lyt_content.removeAllViews();

        webView.clearHistory();

        // NOTE: clears RAM cache, if you pass true, it will also clear the disk cache.
        // Probably not a great idea to pass true if you have other WebViews still alive.
        webView.clearCache(true);

        // Loading a blank page is optional, but will ensure that the WebView isn't doing anything when you destroy it.
        webView.loadUrl("about:blank");

        webView.onPause();
        webView.removeAllViews();
        webView.destroyDrawingCache();

        // NOTE: This pauses JavaScript execution for ALL WebViews,
        // do not use if you have other WebViews still alive.
        // If you create another WebView after calling this,
        // make sure to call mWebView.resumeTimers().
        webView.pauseTimers();

        // NOTE: This can occasionally cause a segfault below API 17 (4.2)
        webView.destroy();

        // Null out the reference so that you don't end up re-using it.
        webView = null;
    }
}
