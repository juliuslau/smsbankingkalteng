package bankkalteng.mobile.com.smsbanking.activities;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.text.method.KeyListener;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import bankkalteng.mobile.com.smsbanking.R;
import bankkalteng.mobile.com.smsbanking.SMSListener;
import bankkalteng.mobile.com.smsbanking.SMSReceiver;

/**
 * Created by MOBILEDEV on 1/21/2017.
 */
public class PurchaseElectricityActivity extends AppCompatActivity {
    public static final String PREFS_NAME = "App_Pref";
    int selectedIdRbDenom;
    String responseKONTEN, NO_HP, FORMAT_SMS,defaultAccountType,defaultProtocol, REK,ussdCode,ORDER_ID,NOMINAL,KONTEN_SMS;
    Button btnSubmit,btnIndex;
    AutoCompleteTextView sourceAccount,denom;
    EditText txtOrderId;
    SharedPreferences sharedPreferences;
    String defaultrekIndex = "1";
    RadioGroup rbToken, selectedRadioGroup;
    IntentFilter intentFilter;
    SweetAlertDialog loading;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        intentFilter = new IntentFilter();
        intentFilter.addAction("SMS_RECEIVED_ACTION");
        NO_HP = getResources().getString(R.string.NO_HP);
        FORMAT_SMS = getResources().getString(R.string.SMS_PEMBELIAN_PULSA_LISTRIK);
        sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        defaultAccountType= sharedPreferences.getString("typeAkun", "");
        defaultProtocol = sharedPreferences.getString("jalurProtocol", "");
        final String[] sourceAcct={"1","2","3","4","5"};
        String[] denomToken={"20000","50000","100000","200000","500000","1000000"};



        setContentView(R.layout.purchase_electricity_layout);
        rbToken = (RadioGroup)findViewById(R.id.rgToken);
        selectedRadioGroup = rbToken;

        btnIndex = (Button)findViewById(R.id.btnIndex);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        sourceAccount = (AutoCompleteTextView)findViewById(R.id.txt_sorceacct);
//        denom = (AutoCompleteTextView)findViewById(R.id.txt_denom);
        txtOrderId = (EditText) findViewById(R.id.txt_orderId);


        if(defaultAccountType.equalsIgnoreCase("Satu Akun")){
            sourceAccount.setFilters(new InputFilter[]{new InputFilter.LengthFilter(35) {

            }});
            sourceAccount.setText("index rekening 1");
            sourceAccount.setTextSize(14);
            sourceAccount.setClickable(false);
            sourceAccount.setCursorVisible(false);
            sourceAccount.setEnabled(false);

        }else{
            ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,sourceAcct);
            sourceAccount.setAdapter(adapter);
            sourceAccount.setThreshold(1);
            sourceAccount.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1) {

            }});
        }

        sourceAccount.setInputType(InputType.TYPE_CLASS_PHONE);
        txtOrderId.setInputType(InputType.TYPE_CLASS_PHONE);
//        denom.setInputType(InputType.TYPE_CLASS_PHONE);

        KeyListener keyListener = DigitsKeyListener.getInstance("1234567890");
        sourceAccount.setKeyListener(keyListener);
        txtOrderId.setKeyListener(keyListener);
//        denom.setKeyListener(keyListener);

//        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,denomToken);
//        denom.setAdapter(adapter);
//        denom.setThreshold(1);

        btnIndex.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                KONTEN_SMS = getResources().getString(R.string.SMS_INDEX_REKENING);
                sendSMS(NO_HP,KONTEN_SMS);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(defaultAccountType.equalsIgnoreCase("Satu Akun")) {
                        if (defaultProtocol.equalsIgnoreCase("SMS")) {
                            if (txtOrderId.getText().toString().equalsIgnoreCase("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(PurchaseElectricityActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan ID pelanggan !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                txtOrderId.setError("");

                                return;
                            } if (txtOrderId.getText().toString().length() < 11) {
                                SweetAlertDialog loading = new SweetAlertDialog(PurchaseElectricityActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan ID pelanggan dengan benar !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                txtOrderId.setError("");
                                return;
                            }if(selectedRadioGroup.getCheckedRadioButtonId() == -1){
                                SweetAlertDialog loading = new SweetAlertDialog(PurchaseElectricityActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Silahkan pilih nominal token !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                return;
                            }else{

                                selectedIdRbDenom = selectedRadioGroup.getCheckedRadioButtonId();
                                RadioButton radioButton = (RadioButton) findViewById(selectedIdRbDenom);

                                String finalDenom;
                                finalDenom = radioButton.getText().toString();
                                if (finalDenom.equalsIgnoreCase("20rb")){
                                    NOMINAL = "20000";
                                }if(finalDenom.equalsIgnoreCase("50rb")){
                                    NOMINAL = "50000";
                                }if(finalDenom.equalsIgnoreCase("100rb")){
                                    NOMINAL = "100000";
                                }if(finalDenom.equalsIgnoreCase("200rb")){
                                    NOMINAL = "200000";
                                }if(finalDenom.equalsIgnoreCase("500rb")){
                                    NOMINAL = "500000";
                                }if(finalDenom.equalsIgnoreCase("1jt")){
                                    NOMINAL = "1000000";
                                }

                                ORDER_ID = txtOrderId.getText().toString();
                                REK = defaultrekIndex;
                                KONTEN_SMS = FORMAT_SMS + " " + REK + " " + ORDER_ID + " " + NOMINAL;
                                sendSMS(NO_HP,KONTEN_SMS);

                            }

                        }else{
                            ussdCode = "*" + "141" + "*" + "125" + Uri.encode("#");
                            if (ActivityCompat.checkSelfPermission(PurchaseElectricityActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            if (ActivityCompat.checkSelfPermission(PurchaseElectricityActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
                        }
                    }else{

                        if (defaultProtocol.equalsIgnoreCase("SMS")) {
                            if (sourceAccount.getText().toString().equalsIgnoreCase("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(PurchaseElectricityActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan index rekening !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                sourceAccount.setError("");
                                return;
                            }
                            if (txtOrderId.getText().toString().equalsIgnoreCase("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(PurchaseElectricityActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan ID pelanggan !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                txtOrderId.setError("");
                                return;
                            }if (txtOrderId.getText().toString().length() < 11) {
                                SweetAlertDialog loading = new SweetAlertDialog(PurchaseElectricityActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan ID pelanggan dengan benar !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                txtOrderId.setError("");
                                    return;

                            }if(selectedRadioGroup.getCheckedRadioButtonId() == -1){
                                SweetAlertDialog loading = new SweetAlertDialog(PurchaseElectricityActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Silahkan pilih nominal token !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                return;


                            }else{

                                selectedIdRbDenom = selectedRadioGroup.getCheckedRadioButtonId();
                                RadioButton radioButton = (RadioButton) findViewById(selectedIdRbDenom);

                                String finalDenom;
                                finalDenom = radioButton.getText().toString();
                                if (finalDenom.equalsIgnoreCase("20rb")){
                                    NOMINAL = "20000";
                                }if(finalDenom.equalsIgnoreCase("50rb")){
                                    NOMINAL = "50000";
                                }if(finalDenom.equalsIgnoreCase("100rb")){
                                    NOMINAL = "100000";
                                }if(finalDenom.equalsIgnoreCase("200rb")){
                                    NOMINAL = "200000";
                                }if(finalDenom.equalsIgnoreCase("500rb")){
                                    NOMINAL = "500000";
                                }if(finalDenom.equalsIgnoreCase("1jt")){
                                    NOMINAL = "1000000";
                                }

                                ORDER_ID = txtOrderId.getText().toString();
                                REK = sourceAccount.getText().toString();
                                KONTEN_SMS = FORMAT_SMS + " " + REK + " " + ORDER_ID + " " + NOMINAL;
                                sendSMS(NO_HP,KONTEN_SMS);

                            }
                        }else{
                            ussdCode = "*" + "141" + "*" + "5" + "*" + "2" + "*" + REK + "*" + ORDER_ID + "*" + NOMINAL + Uri.encode("#");
                            if (ActivityCompat.checkSelfPermission(PurchaseElectricityActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            if (ActivityCompat.checkSelfPermission(PurchaseElectricityActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        SMSReceiver.bindListener(new SMSListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void messageReceived(String messageText) {

                final Matcher matcher = Pattern.compile("disertai digit").matcher(messageText);

                if(matcher.find()){
                    AlertDialog.Builder alert = new AlertDialog.Builder(PurchaseElectricityActivity.this);
                    alert.setMessage(messageText); //Message here

                    LinearLayout layout = new LinearLayout(PurchaseElectricityActivity.this);
                    layout.setOrientation(LinearLayout.VERTICAL);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(20, 0, 30, 0);

                    final EditText input = new EditText(PurchaseElectricityActivity.this);
                    layout.addView(input,params);
                    input.setInputType(InputType.TYPE_CLASS_TEXT);


                    input.setMaxLines(5);
                    input.setMaxWidth(50);
                    input.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5) {

                    }});


                    alert.setView(layout);


                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            responseKONTEN = input.getText().toString();

                            if (input.getText().length()==0){
                                input.setError("masukan index rekening");
                                return;
                            }else {

                                sendSMS(NO_HP,responseKONTEN);
                                return;
                            }


                        } // End of onClick(DialogInterface dialog, int whichButton)
                    }); //End of alert.setPositiveButton


                    alert.setCancelable(false);
                    alert.show();
                    return;

                }else{
                    new SweetAlertDialog(PurchaseElectricityActivity.this, SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText("")
                            .setContentText(messageText)
                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    return;
                }

            }
        });

    }



    public void ClearField(){
        if(defaultAccountType.equalsIgnoreCase("Satu Akun")){
            sourceAccount.setFilters(new InputFilter[]{new InputFilter.LengthFilter(35) {

            }});
            sourceAccount.setText("Anda menggunakan index rekening 1");
            sourceAccount.setTextSize(12);
            sourceAccount.setClickable(false);
            sourceAccount.setCursorVisible(false);
            sourceAccount.setEnabled(false);
        }else{
            sourceAccount.getText().clear();
        }
        rbToken.clearCheck();
        txtOrderId.getText().clear();

    }



    private void sendSMS(String phoneNumber, String message) {

        loading = new SweetAlertDialog(PurchaseElectricityActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setTitleText("Memproses...");

        loading.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                text.setGravity(Gravity.CENTER);
            }
        });
        loading.show();


        new CountDownTimer(30000, 1000) { //40000 milli seconds is total time, 1000 milli seconds is time interval

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                loading.dismiss();
            }
        }.start();




        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
                SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

        registerReceiver(deliverBroadcastReceiver, new IntentFilter(DELIVERED));
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);



    }

    BroadcastReceiver sendBroadcastReceiver = new SentReceiver();
    BroadcastReceiver deliverBroadcastReceiver = new DeliverReceiver();


    class DeliverReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    loading.dismiss();
                    ClearField();
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(getBaseContext(), "Gagal Mengirim",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }


    class SentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "TERKIRIM", Toast.LENGTH_SHORT)
                            .show();
                    loading.dismiss();
                    new SweetAlertDialog(PurchaseElectricityActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Perhatian !")
                            .setContentText("Mohon menunggu sampai anda mendapat informasi balasan dari kami !")

                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Toast.makeText(getBaseContext(), "Generic failure",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Toast.makeText(getBaseContext(), "No service",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT)
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Toast.makeText(getBaseContext(), "Radio off",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }

    @Override
    protected void onResume() {
        registerReceiver(sendBroadcastReceiver,intentFilter);
        super.onResume();
    }

    @Override
    protected void onPause() {

            try {
                unregisterReceiver(sendBroadcastReceiver);
                unregisterReceiver(deliverBroadcastReceiver);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        super.onDestroy();
    }
}
