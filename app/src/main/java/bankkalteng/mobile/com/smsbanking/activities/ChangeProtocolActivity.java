package bankkalteng.mobile.com.smsbanking.activities;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.InputFilter;
import android.text.InputType;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import bankkalteng.mobile.com.smsbanking.R;
import bankkalteng.mobile.com.smsbanking.SMSListener;
import bankkalteng.mobile.com.smsbanking.SMSReceiver;

/**
 * Created by MOBILEDEV on 11/15/2016.
 */
public class ChangeProtocolActivity extends AppCompatActivity {
    public static final String PREFS_NAME = "App_Pref";
    RadioGroup radioGroup;
    RadioButton radioSMS, radioUSSD;
    Button btnSave, btnBack;
    String defautProtocol;
    SharedPreferences sharedPreferences;
    SweetAlertDialog loading;
    IntentFilter intentFilter;
    public String responseKONTEN,NO_HP;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.change_protocol_layout);
        NO_HP = getResources().getString(R.string.NO_HP);
        sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        intentFilter = new IntentFilter();
        intentFilter.addAction("SMS_RECEIVED_ACTION");
        defautProtocol = sharedPreferences.getString("jalurProtocol", "");

        radioGroup = (RadioGroup)findViewById(R.id.radioGroupProtocol);
        radioSMS = (RadioButton)findViewById(R.id.radioSMS);
        radioUSSD = (RadioButton)findViewById(R.id.radioUSSD);
        btnSave = (Button)findViewById(R.id.btnSubmit);



        if(defautProtocol.equalsIgnoreCase("sms")){
            radioSMS.setChecked(true);

        }else if(defautProtocol.equalsIgnoreCase("ussd")){
            radioUSSD.setChecked(true);
        }



        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                if (radioSMS.isChecked()) {

                    defautProtocol = "SMS";
                } else if (radioUSSD.isChecked()) {
                    defautProtocol = "USSD";

                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                SweetAlertDialog loading = new SweetAlertDialog(ChangeProtocolActivity.this, SweetAlertDialog.WARNING_TYPE);


                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                    @Override
                    public void onShow(DialogInterface dialog) {
                        final SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                        text.setGravity(Gravity.CENTER);
                        alertDialog.setTitleText("Apakah anda yakin merubah jalur komunikasi ?");
                                alertDialog.setCancelText("Tidak");
                                alertDialog.setConfirmText("Ya");
                                alertDialog.showCancelButton(true);
                                alertDialog.setCancelClickListener(null);
                        alertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("jalurProtocol", defautProtocol);
                                editor.commit();


                                sweetAlertDialog = new SweetAlertDialog(ChangeProtocolActivity.this, SweetAlertDialog.SUCCESS_TYPE);


                                sweetAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        alertDialog.dismiss();
                                        final SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);

                                        alertDialog.setTitleText("Jalur komunikasi telah dirubah");
                                        alertDialog.setConfirmText("Selesai");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                sweetAlertDialog.show();

                            }
                        });
                    }
                });

                loading.show();

            }
        });


        SMSReceiver.bindListener(new SMSListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void messageReceived(String messageText) {

                final Matcher matcher = Pattern.compile("disertai digit").matcher(messageText);
                if(matcher.find()){
                    AlertDialog.Builder alert = new AlertDialog.Builder(ChangeProtocolActivity.this);
                    alert.setMessage(messageText); //Message here

                    LinearLayout layout = new LinearLayout(ChangeProtocolActivity.this);
                    layout.setOrientation(LinearLayout.VERTICAL);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(20, 0, 30, 0);

                    final EditText input = new EditText(ChangeProtocolActivity.this);
                    layout.addView(input,params);
                    input.setInputType(InputType.TYPE_CLASS_TEXT);


                    input.setMaxLines(5);
                    input.setMaxWidth(50);
                    input.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5) {

                    }});


                    alert.setView(layout);


                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            responseKONTEN = input.getText().toString();

                            if (input.getText().length()==0){
                                input.setError("masukan index rekening");
                                return;
                            }else {

                                sendSMS(NO_HP,responseKONTEN);
                                return;
                            }


                        } // End of onClick(DialogInterface dialog, int whichButton)
                    }); //End of alert.setPositiveButton


                    alert.setCancelable(true);
                    alert.show();

                }else{

                    new SweetAlertDialog(ChangeProtocolActivity.this, SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText("")
                            .setContentText(messageText)
                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                }

            }
        });





    }


    private void sendSMS(String phoneNumber, String message) {

        loading = new SweetAlertDialog(ChangeProtocolActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setTitleText("Memproses...");

        loading.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                text.setGravity(Gravity.CENTER);
            }
        });
        loading.show();


        new CountDownTimer(30000, 1000) { //40000 milli seconds is total time, 1000 milli seconds is time interval

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                loading.dismiss();
            }
        }.start();




        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
                SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

        registerReceiver(deliverBroadcastReceiver, new IntentFilter(DELIVERED));
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);



    }

    BroadcastReceiver sendBroadcastReceiver = new SentReceiver();
    BroadcastReceiver deliverBroadcastReceiver = new DeliverReceiver();


    class DeliverReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:

                    loading.dismiss();
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(getBaseContext(), "Gagal Terkirimm",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }


    class SentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "Mengirim", Toast.LENGTH_SHORT)
                            .show();
                    loading.dismiss();
                    new SweetAlertDialog(ChangeProtocolActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Perhatian !")
                            .setContentText("Mohon menunggu sampai anda mendapat informasi balasan dari kami !")

                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Toast.makeText(getBaseContext(), "Generic failure",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Toast.makeText(getBaseContext(), "No service",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT)
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Toast.makeText(getBaseContext(), "Radio off",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }

    @Override
    protected void onPause() {
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        registerReceiver(sendBroadcastReceiver,intentFilter);


        super.onResume();
    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        super.onDestroy();
    }


    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent a = new Intent(this,SettingActivity.class);
            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(a);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
