package bankkalteng.mobile.com.smsbanking.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fr.rolandl.carousel.CarouselAdapter;
import fr.rolandl.carousel.CarouselItem;
import bankkalteng.mobile.com.smsbanking.R;
import bankkalteng.mobile.com.smsbanking.models.WheelItem;

/**
 * Created by rpj on 17/03/17.
 */

public final class WheelAdapter extends CarouselAdapter<WheelItem> {

    public static final class MenuItem extends CarouselItem<WheelItem>{
        private ImageView image;

        private TextView name;

        private Context context;


        public MenuItem(Context context){
            super(context, R.layout.wheel_item);
            this.context = context;
        }

        @Override
        public void extractView(View view) {
            image = (ImageView) view.findViewById(R.id.image);
            name = (TextView) view.findViewById(R.id.name);
        }

        @Override
        public void update(WheelItem wheelItem) {
            image.setImageResource(getResources().getIdentifier(wheelItem.image, "drawable", context.getPackageName()));
            name.setText(wheelItem.name);
        }
    }

    public WheelAdapter(Context context, List<WheelItem> wheelItems)
    {
        super(context, wheelItems);
    }

    @Override
    public CarouselItem<WheelItem> getCarouselItem(Context context) {
        return new MenuItem(context);
    }


}
