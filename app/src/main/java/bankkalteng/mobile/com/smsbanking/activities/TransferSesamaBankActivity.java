package bankkalteng.mobile.com.smsbanking.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import bankkalteng.mobile.com.smsbanking.R;

/**
 * Created by MOBILEDEV on 1/17/2017.
 */
public class TransferSesamaBankActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.transfer_sesama_bank_activity_layout);
    }
}
