package bankkalteng.mobile.com.smsbanking.coreclass;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import bankkalteng.mobile.com.smsbanking.BuildConfig;
import timber.log.Timber;

public class CoreApp extends Application {



    public static String gcmId;
    private static Context mContext;


    private static CoreApp instance;

    public static CoreApp getInstance(){
        return instance;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        mContext = this;

        initTimber();


    }

    public static Context getContext(){ return mContext;  }


    private void initTimber() {
        if (BuildConfig.DEBUG)
            Timber.plant(new Timber.DebugTree());
        Timber.d("Init Timber Done");

    }

}
