package bankkalteng.mobile.com.smsbanking.activities;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import bankkalteng.mobile.com.smsbanking.R;

/**
 * Created by MOBILEDEV on 1/21/2017.
 */
public class PaymentElecticity extends AppCompatActivity {
    public static final String PREFS_NAME = "App_Pref";
    String responseKONTEN, NO_HP, FORMAT_SMS,defaultAccountType,defaultProtocol, REK,ussdCode,ORDER_ID;
    Button btnSubmit;
    AutoCompleteTextView sourceAccount;
    EditText txtOrderId;
    SharedPreferences sharedPreferences;
    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NO_HP = getResources().getString(R.string.NO_HP);
        FORMAT_SMS = getResources().getString(R.string.SMS_PEMBELIAN_PULSA);
        sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        defaultAccountType= sharedPreferences.getString("typeAkun", "");
        defaultProtocol = sharedPreferences.getString("jalurProtocol", "");
        String[] sourceAcct={"1","2","3","4","5"};


        setContentView(R.layout.purchase_pulsa_layout_);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        sourceAccount = (AutoCompleteTextView)findViewById(R.id.txt_sorceacct);
        txtOrderId = (EditText) findViewById(R.id.txt_oder_id);


        if(defaultAccountType.equalsIgnoreCase("Satu Akun")){
            sourceAccount.setText("Anda menggunakan index rekening 1");
            sourceAccount.setTextSize(12);
            sourceAccount.setClickable(false);
            sourceAccount.setCursorVisible(false);
            sourceAccount.setEnabled(false);

        }else{
            ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,sourceAcct);
            sourceAccount.setAdapter(adapter);
            sourceAccount.setThreshold(1);
        }



        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(defaultAccountType.equalsIgnoreCase("Satu Akun")) {
                        if (defaultProtocol.equalsIgnoreCase("SMS")) {

                        }else{
                            ussdCode = "*" + "141" + "*" + "5" + "*"  + REK + "*" + ORDER_ID + "*" + Uri.encode("#");
                            if (ActivityCompat.checkSelfPermission(PaymentElecticity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            if (ActivityCompat.checkSelfPermission(PaymentElecticity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
                        }
                    }else{

                        if (defaultProtocol.equalsIgnoreCase("SMS")) {

                        }else{
                            ussdCode = "*" + "141" + "*" + "5" + "*"  + REK + "*" + ORDER_ID + "*" + Uri.encode("#");
                            if (ActivityCompat.checkSelfPermission(PaymentElecticity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            if (ActivityCompat.checkSelfPermission(PaymentElecticity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    protected void sendSMSMessage(String NOHP, String KONTENSMS) {

        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(NOHP, null, KONTENSMS, null, null);


            new SweetAlertDialog(PaymentElecticity.this, SweetAlertDialog.SUCCESS_TYPE)
                    .setContentText("Cek pesan masuk")
                    .setTitleText("Berhasil")
                    .setCancelText("Tidak")
                    .setConfirmText("Ya")
                    .showCancelButton(true)
                    .setCancelClickListener(null)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            openInbox();
                        }
                    })
                    .show();

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(),
                    "SMS gagal, silahkan coba kembali!",
                    Toast.LENGTH_LONG).show();


            e.printStackTrace();
        }
    }


    public void openInbox() {
        String application_name = "com.android.mms";
        try {
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.LAUNCHER");

            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            List<ResolveInfo> resolveinfo_list = this.getPackageManager()
                    .queryIntentActivities(intent, 0);

            for (ResolveInfo info : resolveinfo_list) {
                if (info.activityInfo.packageName
                        .equalsIgnoreCase(application_name)) {
                    launchComponent(info.activityInfo.packageName,
                            info.activityInfo.name);
                    break;
                }
            }
        } catch (ActivityNotFoundException e) {
            Toast.makeText(
                    this.getApplicationContext(),
                    "There was a problem loading the application: "
                            + application_name, Toast.LENGTH_SHORT).show();
        }
    }

    private void launchComponent(String packageName, String name) {
        Intent launch_intent = new Intent("android.intent.action.MAIN");
        launch_intent.addCategory("android.intent.category.LAUNCHER");
        launch_intent.setComponent(new ComponentName(packageName, name));
        launch_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(launch_intent);
    }
}
