package bankkalteng.mobile.com.smsbanking.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;

import it.moondroid.coverflow.components.ui.containers.FeatureCoverFlow;
import bankkalteng.mobile.com.smsbanking.R;
import bankkalteng.mobile.com.smsbanking.adapter.CarouselMenuAdapter;

/**
 * Created by rpj on 15/03/17.
 */

public class CarouselMenu extends AppCompatActivity {

    private FeatureCoverFlow coverFlow;
    private CarouselMenuAdapter adapter;
    private ArrayList<bankkalteng.mobile.com.smsbanking.models.CarouselMenu> carouselMenus;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.carosel_menu_layout);
        coverFlow = (FeatureCoverFlow) findViewById(R.id.coverflow);

        settingDummyData();
        adapter = new CarouselMenuAdapter(this,carouselMenus);
        coverFlow.setAdapter(adapter);
        coverFlow.setOnScrollPositionListener(onScrollListener());
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private FeatureCoverFlow.OnScrollPositionListener onScrollListener() {
        return new FeatureCoverFlow.OnScrollPositionListener() {
            @Override
            public void onScrolledToPosition(int position) {
                Log.v("MainActiivty", "position: " + position);
            }

            @Override
            public void onScrolling() {
                Log.i("MainActivity", "scrolling");
            }
        };
    }

    private void settingDummyData() {
        carouselMenus = new ArrayList<>();
        carouselMenus.add(new bankkalteng.mobile.com.smsbanking.models.CarouselMenu(R.drawable.ic_acc_info_selected ,"Rekening"));
        carouselMenus.add(new bankkalteng.mobile.com.smsbanking.models.CarouselMenu(R.mipmap.ic_account_, "Info"));
        carouselMenus.add(new bankkalteng.mobile.com.smsbanking.models.CarouselMenu(R.mipmap.ic_acc, "Call Of Duty Black Ops 3"));
        carouselMenus.add(new bankkalteng.mobile.com.smsbanking.models.CarouselMenu(R.mipmap.ic_account_, "DotA 2"));
        carouselMenus.add(new bankkalteng.mobile.com.smsbanking.models.CarouselMenu(R.mipmap.ic_acc, "Halo 5"));
       carouselMenus.add(new bankkalteng.mobile.com.smsbanking.models.CarouselMenu(R.mipmap.ic_account_, "StarCraft"));
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("CarouselMenu Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}
