package bankkalteng.mobile.com.smsbanking.activities;

import android.app.Activity;
import android.app.ListActivity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.InputFilter;
import android.text.InputType;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import bankkalteng.mobile.com.smsbanking.R;
import bankkalteng.mobile.com.smsbanking.SMSListener;
import bankkalteng.mobile.com.smsbanking.SMSReceiver;

/**
 * Created by MOBILEDEV on 10/27/2016.
 */
public class SettingActivity extends AppCompatActivity {
    public static final String PREFS_NAME = "App_Pref";
    private ListActivity listActivity;
    public String settingCategory,responseKONTEN,NO_HP;
    String valueJalurKomunikasi = "SMS";
    IntentFilter intentFilter;
    String userProtocol;
    SharedPreferences sharedPreferences;
    SweetAlertDialog loading;
    String[] listData, listDataUSSD;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listActivity = new ListActivity();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.purchase_layout);
        // storing string resources into Array
        listData = getResources().getStringArray(R.array.setting_list);
        listDataUSSD = getResources().getStringArray(R.array.list_setting_ussd);
        intentFilter = new IntentFilter();
        intentFilter.addAction("SMS_RECEIVED_ACTION");
        // Binding resources Array to ListAdapter
        NO_HP = getResources().getString(R.string.NO_HP);
        sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        userProtocol = sharedPreferences.getString("jalurProtocol", "");


        if (userProtocol.equalsIgnoreCase(" ") || userProtocol.isEmpty()) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("jalurProtocol", valueJalurKomunikasi);

            editor.commit();
        }

        if(userProtocol.equals("USSD")){
            ArrayAdapter<String> adapter =new ArrayAdapter<String>(this, R.layout.list_item_arrow, R.id.label, listDataUSSD);
            ListView lv = (ListView)findViewById(R.id.lv);
            lv.setAdapter(adapter);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {

                    settingCategory=(String)parent.getItemAtPosition(position);
                   if(settingCategory.equalsIgnoreCase("Jalur Komunikasi")){
                        Intent intent = new Intent(SettingActivity.this, ChangeProtocolActivity.class);
                        intent.putExtra("settingCategory", settingCategory);
                        startActivity(intent);
                    }


                }
            });
        }else{
            ArrayAdapter<String> adapter =new ArrayAdapter<String>(this, R.layout.list_item_arrow, R.id.label, listData);
            ListView lv = (ListView)findViewById(R.id.lv);
            lv.setAdapter(adapter);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {

                    settingCategory=(String)parent.getItemAtPosition(position);
                    if(settingCategory.equalsIgnoreCase("Ganti PIN")){
                        Intent intent = new Intent(SettingActivity.this, GantiPIN.class);
                        intent.putExtra("settingCategory", settingCategory);
                        startActivity(intent);
                    }if(settingCategory.equalsIgnoreCase("Akun")){
                        Intent intent = new Intent(SettingActivity.this, SettingAkun.class);
                        intent.putExtra("settingCategory", settingCategory);
                        startActivity(intent);
                    }if(settingCategory.equalsIgnoreCase("Jalur Komunikasi")){
                        Intent intent = new Intent(SettingActivity.this, ChangeProtocolActivity.class);
                        intent.putExtra("settingCategory", settingCategory);
                        startActivity(intent);
                    }


                }
            });
        }





        // listening to single list item on click



        SMSReceiver.bindListener(new SMSListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void messageReceived(String messageText) {

                final Matcher matcher = Pattern.compile("disertai digit").matcher(messageText);
                if(matcher.find()){
                    AlertDialog.Builder alert = new AlertDialog.Builder(SettingActivity.this);
                    alert.setMessage(messageText); //Message here

                    LinearLayout layout = new LinearLayout(SettingActivity.this);
                    layout.setOrientation(LinearLayout.VERTICAL);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(20, 0, 30, 0);

                    final EditText input = new EditText(SettingActivity.this);
                    layout.addView(input,params);
                    input.setInputType(InputType.TYPE_CLASS_TEXT);


                    input.setMaxLines(5);
                    input.setMaxWidth(50);
                    input.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5) {

                    }});


                    alert.setView(layout);


                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            responseKONTEN = input.getText().toString();

                            if (input.getText().length()==0){
                                input.setError("");
                                return;
                            }else {

                                sendSMS(NO_HP,responseKONTEN);
                                return;
                            }


                        } // End of onClick(DialogInterface dialog, int whichButton)
                    }); //End of alert.setPositiveButton


                    alert.setCancelable(true);
                    alert.show();

                }else{

                    new SweetAlertDialog(SettingActivity.this, SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText("")
                            .setContentText(messageText)
                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                }

            }
        });
    }

    private void sendSMS(String phoneNumber, String message) {

        loading = new SweetAlertDialog(SettingActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setTitleText("Memproses...");

        loading.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                text.setGravity(Gravity.CENTER);
            }
        });
        loading.show();


        new CountDownTimer(30000, 1000) { //40000 milli seconds is total time, 1000 milli seconds is time interval

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                loading.dismiss();
            }
        }.start();




        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
                SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

        registerReceiver(deliverBroadcastReceiver, new IntentFilter(DELIVERED));
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);



    }

    BroadcastReceiver sendBroadcastReceiver = new SentReceiver();
    BroadcastReceiver deliverBroadcastReceiver = new DeliverReceiver();


    class DeliverReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:

                    loading.dismiss();
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(getBaseContext(), "Gagal Terkirimm",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }


    class SentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "Mengirim", Toast.LENGTH_SHORT)
                            .show();
                    loading.dismiss();
                    new SweetAlertDialog(SettingActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Perhatian !")
                            .setContentText("Mohon menunggu sampai anda mendapat informasi balasan dari kami !")

                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Toast.makeText(getBaseContext(), "Generic failure",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Toast.makeText(getBaseContext(), "No service",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT)
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Toast.makeText(getBaseContext(), "Radio off",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }

    @Override
    protected void onRestart() {

        super.onRestart();
    }

    @Override
    protected void onResume() {

        registerReceiver(sendBroadcastReceiver,intentFilter);

        super.onPostResume();
    }

    @Override
    protected void onPause() {
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent a = new Intent(this,MainActivity.class);
            a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(a);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}


