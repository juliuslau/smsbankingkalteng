package bankkalteng.mobile.com.smsbanking.activities;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.text.method.KeyListener;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import bankkalteng.mobile.com.smsbanking.AppHelper;
import bankkalteng.mobile.com.smsbanking.R;
import bankkalteng.mobile.com.smsbanking.SMSListener;
import bankkalteng.mobile.com.smsbanking.SMSReceiver;

/**
 * Created by MOBILEDEV on 10/26/2016.
 */
public class AccountActivity extends AppCompatActivity {
    public static final String PREFS_NAME = "App_Pref";
    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS =0 ;
    Button btnAccInfo, btnAccStatement;
//    EditText input;
    public String defaultProtocol,ussdCode,defaultAkunType;;
    private static final int PERMISSION_REQUEST = 100;
    MainActivity mainActivity;
    Processors processors;
    SharedPreferences sharedPreferences;
    LayoutInflater inflater;
    View dialogView;
    EditText inputs;
    public String responseKONTEN, NO_HP, KONTEN_SMS, PIN, KODE_REK, USER_PIN,FORMAT_SMS,USER_INDEX,phoneNo,message;
    public String defaultIndex = "1";
    final Timer timer = new Timer();

    IntentFilter intentFilter;

    SweetAlertDialog loading;



    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_account);
        overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_in);

        intentFilter = new IntentFilter();
        intentFilter.addAction("SMS_RECEIVED_ACTION");

        btnAccInfo = (Button) findViewById(R.id.menu_acc_info);
        btnAccStatement= (Button) findViewById(R.id.menu_acc_statement);

        Animation anim_left = AnimationUtils.loadAnimation(getApplication(), R.anim.push_left);
        Animation anim_right = AnimationUtils.loadAnimation(getApplication(), R.anim.push_right);
        btnAccInfo.startAnimation(anim_left);
        btnAccStatement.startAnimation(anim_right);

        KODE_REK = getResources().getString(R.string.KODE_REK);
        NO_HP = getResources().getString(R.string.NO_HP);
        USER_PIN = AppHelper.getPIN(getApplication());


        btnAccInfo.setOnClickListener(myButtonListener);
        btnAccStatement.setOnClickListener(myButtonListener);

        sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        defaultProtocol = sharedPreferences.getString("jalurProtocol", "");
        defaultAkunType = sharedPreferences.getString("typeAkun", "");


        SMSReceiver.bindListener(new SMSListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void messageReceived(String messageText) {

                final Matcher matcher = Pattern.compile("disertai digit").matcher(messageText);
                if(matcher.find()){
                    AlertDialog.Builder alert = new AlertDialog.Builder(AccountActivity.this);
                    alert.setMessage(messageText); //Message here

                    LinearLayout layout = new LinearLayout(AccountActivity.this);
                    layout.setOrientation(LinearLayout.VERTICAL);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(20, 0, 30, 0);

                    final EditText input = new EditText(AccountActivity.this);
                    layout.addView(input,params);
                    input.setInputType(InputType.TYPE_CLASS_TEXT);


                    input.setMaxLines(5);
                    input.setMaxWidth(50);
                    input.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5) {

                    }});


                    alert.setView(layout);


                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            responseKONTEN = input.getText().toString();

                            if (input.getText().length()==0){
                                input.setError("masukan index rekening");
                                return;
                            }else {

                                sendSMS(NO_HP,responseKONTEN);
                                return;
                            }


                        } // End of onClick(DialogInterface dialog, int whichButton)
                    }); //End of alert.setPositiveButton


                    alert.setCancelable(true);
                    alert.show();

                }else{

                    new SweetAlertDialog(AccountActivity.this, SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText("")
                            .setContentText(messageText)
                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                }

            }
        });

    }



    private View.OnClickListener myButtonListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.menu_acc_info:
                    FORMAT_SMS = getResources().getString(R.string.SMS_SALDO);

                    if(defaultAkunType.equalsIgnoreCase("Satu Akun")){

                        if(defaultProtocol.equalsIgnoreCase("sms") ) {
                            KONTEN_SMS = FORMAT_SMS + " " + defaultIndex;

                            sendSMS(NO_HP,KONTEN_SMS);
                        }else{
                            ussdCode = "*" + "141" + "*" + "1" + "*" + "1" +  Uri.encode("#");
                            if (ActivityCompat.checkSelfPermission(AccountActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            if (ActivityCompat.checkSelfPermission(AccountActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
                        }


                    }else {

                        if (defaultProtocol.equalsIgnoreCase("sms")) {

                            AlertDialog.Builder alert = new AlertDialog.Builder(AccountActivity.this);
                            alert.setMessage("Masukan index rekening anda !"); //Message here

                            LinearLayout layout = new LinearLayout(AccountActivity.this);
                            layout.setOrientation(LinearLayout.VERTICAL);
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            params.setMargins(20, 0, 30, 0);

                            final EditText input = new EditText(AccountActivity.this);

                            layout.addView(input,params);
                            input.setInputType(InputType.TYPE_CLASS_PHONE);
                            KeyListener keyListener = DigitsKeyListener.getInstance("1234567890");
                            input.setKeyListener(keyListener);
                            input.setMaxLines(1);
                            input.setMaxWidth(50);
                            input.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                            input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1) {

                            }});

                            alert.setView(layout);
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(input.getWindowToken(), 0);

                            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    USER_INDEX = input.getText().toString();
                                    KONTEN_SMS = FORMAT_SMS+ " " + USER_INDEX;
                                    if (input.getText().length()==0){
                                        input.setError("masukan index rekening");
                                        return;
                                    }else {
                                        sendSMS(NO_HP,KONTEN_SMS);
                                    }
//

                                } // End of onClick(DialogInterface dialog, int whichButton)
                            }); //End of alert.setPositiveButton
                            alert.setNegativeButton("Batal", null); //End of alert.setNegativeButton
                            AlertDialog alertDialog = alert.create();
                            alert.show();
                            break;



                        }else{

                            ussdCode = "*" + "141" + "*" + "1" + "*" + USER_INDEX +  Uri.encode("#");
                            if (ActivityCompat.checkSelfPermission(AccountActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            if (ActivityCompat.checkSelfPermission(AccountActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
                        }
                    }




                    break;
                case R.id.menu_acc_statement:
                    FORMAT_SMS = getResources().getString(R.string.SMS_MUTASI_REKENING);

                    if(defaultAkunType.equalsIgnoreCase("Satu Akun")){

                        if(defaultProtocol.equalsIgnoreCase("sms") ) {
                            KONTEN_SMS = FORMAT_SMS + " " + defaultIndex;
                            sendSMS(NO_HP,KONTEN_SMS);


                        }else{
                            ussdCode = "*" + "141" + "*" + "2" + "*" + "1" +  Uri.encode("#");
                            if (ActivityCompat.checkSelfPermission(AccountActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            if (ActivityCompat.checkSelfPermission(AccountActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
                        }


                    }else {

                        if (defaultProtocol.equalsIgnoreCase("sms")) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(AccountActivity.this);
                            alert.setMessage("Masukan index rekening anda !"); //Message here

                            LinearLayout layout = new LinearLayout(AccountActivity.this);
                            layout.setOrientation(LinearLayout.VERTICAL);
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            params.setMargins(20, 0, 30, 0);

                            final EditText input = new EditText(AccountActivity.this);

                            layout.addView(input,params);
                            input.setInputType(InputType.TYPE_CLASS_PHONE);
                            KeyListener keyListener = DigitsKeyListener.getInstance("1234567890");
                            input.setKeyListener(keyListener);
                            input.setMaxLines(1);
                            input.setMaxWidth(50);
                            input.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                            input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1) {

                            }});

                            alert.setView(layout);

                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(input.getWindowToken(), 0);

                            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    USER_INDEX = input.getText().toString();
                                    KONTEN_SMS = FORMAT_SMS+ " " + USER_INDEX;
                                    if (input.getText().length()==0){
                                        input.setError("masukan index rekening");
                                        return;
                                    }else {

                                        sendSMS(NO_HP,KONTEN_SMS);
                                    }
//

                                } // End of onClick(DialogInterface dialog, int whichButton)
                            }); //End of alert.setPositiveButton
                            alert.setNegativeButton("Batal", null); //End of alert.setNegativeButton
                            AlertDialog alertDialog = alert.create();
                            alert.show();


                            break;
                        }else{

                            ussdCode = "*" + "141" + "*" + "125" +  Uri.encode("#");
                            if (ActivityCompat.checkSelfPermission(AccountActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            if (ActivityCompat.checkSelfPermission(AccountActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }
                            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
                        }
                    }

                    break;



            }
        }
    };






//    public void kirimSMS(final String phoneNumber, final String message)
//    {
//
//
//    final SweetAlertDialog loading = new SweetAlertDialog(AccountActivity.this, SweetAlertDialog.PROGRESS_TYPE);
//    loading.setCancelable(false);
//    loading.setTitleText("Memproses...");
//
//    loading.setOnShowListener(new DialogInterface.OnShowListener() {
//        @Override
//        public void onShow(final DialogInterface dialog) {
//            SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
//            TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
//            text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
//            text.setGravity(Gravity.CENTER);
//
//
//            String SENT = "SMS_SENT";
//            String DELIVERED = "SMS_DELIVERED";
//
//            PendingIntent sentPI = PendingIntent.getBroadcast(AccountActivity.this, 0,
//                    new Intent(SENT), 0);
//
//            PendingIntent deliveredPI = PendingIntent.getBroadcast(AccountActivity.this, 0,
//                    new Intent(DELIVERED), 0);
//
//            //---when the SMS has been sent---
//            registerReceiver(new BroadcastReceiver() {
//                @Override
//                public void onReceive(Context arg0, Intent arg1) {
//                    switch (getResultCode()) {
//                        case Activity.RESULT_OK:
//                            Toast.makeText(getBaseContext(), "Mengirim",
//                                    Toast.LENGTH_SHORT).show();
//
//
//                            break;
//                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//                            Toast.makeText(getBaseContext(), "Kartu SIM tidak ada !",
//                                    Toast.LENGTH_SHORT).show();
//
//                            break;
//                        case SmsManager.RESULT_ERROR_NO_SERVICE:
//                            Toast.makeText(getBaseContext(), "Kegagalan servis",
//                                    Toast.LENGTH_SHORT).show();
//                            break;
//                        case SmsManager.RESULT_ERROR_NULL_PDU:
//                            Toast.makeText(getBaseContext(), "PDU tidak tersedia",
//                                    Toast.LENGTH_SHORT).show();
//                            break;
//                        case SmsManager.RESULT_ERROR_RADIO_OFF:
//                            Toast.makeText(getBaseContext(), "Tidak ada sinyal",
//                                    Toast.LENGTH_SHORT).show();
//                            break;
//                    }
//                }
//            }, new IntentFilter(SENT));
//
//            //---when the SMS has been delivered---
//            registerReceiver(new BroadcastReceiver() {
//                @Override
//                public void onReceive(Context arg0, Intent arg1) {
//                    switch (getResultCode()) {
//                        case Activity.RESULT_OK:
//                            dialog.dismiss();
//                            new SweetAlertDialog(AccountActivity.this, SweetAlertDialog.SUCCESS_TYPE)
//                                    .setTitleText("Buka inbox SMS")
//                                    .setCancelText("Tidak")
//                                    .setConfirmText("Ya")
//                                    .showCancelButton(true)
//                                    .setCancelClickListener(null)
//                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                                        @Override
//                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
//                                            openInbox();
//                                            sweetAlertDialog.dismiss();
//                                        }
//                                    })
//                                    .show();
//
//                            break;
//                        case Activity.RESULT_CANCELED:
//                            Toast.makeText(getBaseContext(), "Gagal terkirim",
//                                    Toast.LENGTH_SHORT).show();
//                            break;
//                    }
//                }
//            }, new IntentFilter(DELIVERED));
////            try {
//            SmsManager sms = SmsManager.getDefault();
//            sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
//
////            }catch (Exception e){
////                e.printStackTrace();
//////                SMSExcept();
////            }
//
//        }
//    });
//
//
//    loading.show();
//
//
//    new CountDownTimer(30000, 1000) { //40000 milli seconds is total time, 1000 milli seconds is time interval
//
//        public void onTick(long millisUntilFinished) {
//        }
//
//        public void onFinish() {
//            loading.dismiss();
//        }
//    }.start();
//
//    }



//    public void showChangeLangDialog() {
//        AlertDialog.Builder alert = new AlertDialog.Builder(AccountActivity.this);
//                            alert.setMessage("Masukan index rekening anda !"); //Message here
//
//                            LinearLayout layout = new LinearLayout(AccountActivity.this);
//                            layout.setOrientation(LinearLayout.VERTICAL);
//                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
//                                    LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//                            params.setMargins(20, 0, 30, 0);
//
//                            final EditText input = new EditText(AccountActivity.this);
//
//                            layout.addView(input,params);
//                            input.setInputType(InputType.TYPE_CLASS_PHONE);
//                            KeyListener keyListener = DigitsKeyListener.getInstance("1234567890");
//                            input.setKeyListener(keyListener);
//                            input.setMaxLines(1);
//                            input.setMaxWidth(50);
//                            input.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//                            input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1) {
//
//                            }});
//
//
//                            alert.setView(layout);
//
//
//
//                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
//
//        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int whichButton) {
//                                    USER_INDEX = input.getText().toString();
//                                    KONTEN_SMS = FORMAT_SMS + USER_INDEX;
//                                    if (input.getText().length()==0){
//                                        input.setError("masukan index rekening");
//                                        return;
//                                    }else {
////                                        USER_INDEX = input.getText().toString();
//
////                                        kirimSMS(NO_HP,USER_INDEX);
//                                        sendSMS(NO_HP,USER_INDEX);
//                                        Toast.makeText(getApplicationContext(), "NO hp : " + NO_HP + " " + "KONTEN : " + USER_INDEX,
//                                                Toast.LENGTH_LONG).show();
//                                     }
////
//
//                               } // End of onClick(DialogInterface dialog, int whichButton)
//                           }); //End of alert.setPositiveButton
//                            alert.setNegativeButton("Batal", null); //End of alert.setNegativeButton
//                            AlertDialog alertDialog = alert.create();
//                           alert.show();
//    }








    @Override
    protected void onResume() {

        registerReceiver(sendBroadcastReceiver,intentFilter);

        super.onPostResume();
    }

    @Override
    protected void onPause() {
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    private void sendSMS(String phoneNumber, String message) {

        loading = new SweetAlertDialog(AccountActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setTitleText("Memproses...");

        loading.setOnShowListener(new DialogInterface.OnShowListener() {
                                      @Override
                                      public void onShow(final DialogInterface dialog) {
                                          SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                          TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                          text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                                          text.setGravity(Gravity.CENTER);
                                      }
                                  });
        loading.show();


        new CountDownTimer(30000, 1000) { //40000 milli seconds is total time, 1000 milli seconds is time interval

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                loading.dismiss();
            }
        }.start();




        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
                SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

        registerReceiver(deliverBroadcastReceiver, new IntentFilter(DELIVERED));
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);



    }

    BroadcastReceiver sendBroadcastReceiver = new SentReceiver();
    BroadcastReceiver deliverBroadcastReceiver = new DeliverReceiver();


    class DeliverReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:

                    loading.dismiss();
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(getBaseContext(), "Gagal Terkirimm",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }


    class SentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "Mengirim", Toast.LENGTH_SHORT)
                            .show();
                    loading.dismiss();
                    new SweetAlertDialog(AccountActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Perhatian !")
                            .setContentText("Mohon menunggu sampai anda mendapat informasi balasan dari kami !")

                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Toast.makeText(getBaseContext(), "Generic failure",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Toast.makeText(getBaseContext(), "No service",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT)
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Toast.makeText(getBaseContext(), "Radio off",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }





}
