package bankkalteng.mobile.com.smsbanking.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import bankkalteng.mobile.com.smsbanking.R;

/**
 * Created by MOBILEDEV on 1/19/2017.
 */
public class PurchasePulsaAdapter extends ArrayAdapter<String>{

    private final Activity context;
    private final String[] titleId;
    private final int[] imageId;

    public PurchasePulsaAdapter(Activity context,
                                String[] titleId, int[] imageId) {
        super(context, R.layout.item_list_pulsa,titleId);
        this.context = context;
        this.titleId = titleId;
        this.imageId = imageId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.item_list_pulsa, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.Itemname);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        txtTitle.setText(titleId[position]);

        imageView.setImageResource(imageId[position]);
        return rowView;
    }
}
