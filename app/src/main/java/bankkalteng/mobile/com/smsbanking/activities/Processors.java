package bankkalteng.mobile.com.smsbanking.activities;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsManager;
import android.widget.Toast;

/**
 * Created by MOBILEDEV on 1/16/2017.
 */
public class Processors {

    ProgressDialog pDialog;
    Context context;
    public Processors(Context _context){
        this.context=_context;
    }

    public void checkSaldoSMS(String NO_HP, String KONTEN){
        pDialog = ProgressDialog.show(context, "SMS Banking",
                "sedang memproses ...", true);
        String SENT = "sent";
        String DELIVERED = "delivered";

        Intent sentIntent = new Intent(SENT);
     /*Create Pending Intents*/
        PendingIntent sentPI = PendingIntent.getBroadcast(
                context, 0, sentIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Intent deliveryIntent = new Intent(DELIVERED);

        PendingIntent deliverPI = PendingIntent.getBroadcast(
                context, 0, deliveryIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
     /* Register for SMS send action */
        context.registerReceiver(new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                String result = "";

                switch (getResultCode()) {

                    case Activity.RESULT_OK:
                        result = "Transmission successful";
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        result = "Transmission failed";
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        result = "Radio off";
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        result = "No PDU defined";
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        result = "No service";
                        break;
                }

                Toast.makeText(context, result,
                        Toast.LENGTH_LONG).show();
                pDialog.dismiss();
            }

        }, new IntentFilter(SENT));
     /* Register for Delivery event */
        context.registerReceiver(new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
//                            Toast.makeText(getApplicationContext(), "Deliverd",
//                                    Toast.LENGTH_LONG).show();

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set title
                alertDialogBuilder.setTitle("SMS Banking");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Silahkan cek pesan masuk anda")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
//                                openInbox();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }

        }, new IntentFilter(DELIVERED));

      /*Send SMS*/
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(NO_HP, null, KONTEN, sentPI,
                deliverPI);




        }
    }



