package bankkalteng.mobile.com.smsbanking.activities;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import bankkalteng.mobile.com.smsbanking.R;

/**
 * Created by MOBILEDEV on 12/7/2016.
 */
public class TransferInhouseList extends AppCompatActivity {

    private ListActivity listActivity;
    public String settingCategory;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listActivity = new ListActivity();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.purchase_layout);
        // storing string resources into Array
        String[] listData = getResources().getStringArray(R.array.inhouse_list);

        // Binding resources Array to ListAdapter


        ArrayAdapter<String> adapter =new ArrayAdapter<String>(this, R.layout.list_item_arrow, R.id.label, listData);
        ListView lv = (ListView)findViewById(R.id.lv);
        lv.setAdapter(adapter);



        // listening to single list item on click
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                settingCategory=(String)parent.getItemAtPosition(position);
                if(settingCategory.equalsIgnoreCase("Rekening Sendiri")){
                    Intent intent = new Intent(TransferInhouseList.this, TransferInhouseIndex.class);
                    intent.putExtra("settingCategory", settingCategory);
                    startActivity(intent);
                }if(settingCategory.equalsIgnoreCase("Rekening Tujuan Lain")){
                    Intent intent = new Intent(TransferInhouseList.this, TransferInhouseActivityIndex.class);
intent.putExtra("settingCategory", settingCategory);
        startActivity(intent);
        }


        }
        });
        }
        }
