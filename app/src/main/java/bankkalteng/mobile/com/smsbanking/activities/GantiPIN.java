package bankkalteng.mobile.com.smsbanking.activities;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.InputFilter;
import android.text.InputType;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import bankkalteng.mobile.com.smsbanking.R;
import bankkalteng.mobile.com.smsbanking.SMSListener;
import bankkalteng.mobile.com.smsbanking.SMSReceiver;

/**
 * Created by MOBILEDEV on 1/17/2017.
 */
public class GantiPIN extends AppCompatActivity {
    ProgressDialog pDialog;
    EditText pinLama, pinBaru1, pinBaru2;
    Button btnSubmit;
    String responseKONTEN,NO_HP, FORMAT_SMS, PIN, PIN_BARU1, PIN_BARU2,KONTEN_SMS;
    IntentFilter intentFilter;

    SweetAlertDialog loading;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ganti_pin_layout);
        intentFilter = new IntentFilter();
        intentFilter.addAction("SMS_RECEIVED_ACTION");
        FORMAT_SMS = getResources().getString(R.string.SMS_GANTI_PIN);
        NO_HP =getResources().getString(R.string.NO_HP);
        pinLama = (EditText)findViewById(R.id.txt_pin_lama);
        pinBaru1 = (EditText)findViewById(R.id.txt_pin_baru1);
        pinBaru2 = (EditText)findViewById(R.id.txt_pin_baru2);
        btnSubmit = (Button)findViewById(R.id.btnSubmit);


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (pinLama.getText().toString().equals("") || pinLama.getText().length() < 6 ) {
                        SweetAlertDialog loading = new SweetAlertDialog(GantiPIN.this, SweetAlertDialog.WARNING_TYPE);

                        loading.setOnShowListener(new DialogInterface.OnShowListener() {

                            @Override
                            public void onShow(DialogInterface dialog) {
                                SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                text.setGravity(Gravity.CENTER);
                                alertDialog.setCancelable(false);
                                alertDialog.setTitleText("Masukan PIN anda !");
                                alertDialog.setConfirmText("Ok");
                                alertDialog.setConfirmClickListener(null);
                            }
                        });

                        loading.show();
                        pinLama.setError("");
                        return;
                    } if (pinBaru1.getText().toString().equals("") || pinLama.getText().length() < 6 ) {
                        SweetAlertDialog loading = new SweetAlertDialog(GantiPIN.this, SweetAlertDialog.WARNING_TYPE);

                        loading.setOnShowListener(new DialogInterface.OnShowListener() {

                            @Override
                            public void onShow(DialogInterface dialog) {
                                SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                text.setGravity(Gravity.CENTER);
                                alertDialog.setCancelable(false);
                                alertDialog.setTitleText("Masukan PIN baru !");
                                alertDialog.setConfirmText("Ok");
                                alertDialog.setConfirmClickListener(null);
                            }
                        });

                        loading.show();
                        pinBaru1.setError("");
                        return;
                    }
                    if (pinBaru2.getText().toString().equals("") || pinLama.getText().length() < 6 ) {
                        SweetAlertDialog loading = new SweetAlertDialog(GantiPIN.this, SweetAlertDialog.WARNING_TYPE);

                        loading.setOnShowListener(new DialogInterface.OnShowListener() {

                            @Override
                            public void onShow(DialogInterface dialog) {
                                SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                text.setGravity(Gravity.CENTER);
                                alertDialog.setCancelable(false);
                                alertDialog.setTitleText("Konfirmasi PIN baru !");
                                alertDialog.setConfirmText("Ok");
                                alertDialog.setConfirmClickListener(null);
                            }
                        });

                        loading.show();
                        pinBaru2.setError("");
                        return;

                    }if (!pinBaru1.getText().toString().equals(pinBaru2.getText().toString()) || !pinBaru2.getText().toString().equals(pinBaru1.getText().toString())) {
                        SweetAlertDialog loading = new SweetAlertDialog(GantiPIN.this, SweetAlertDialog.WARNING_TYPE);

                        loading.setOnShowListener(new DialogInterface.OnShowListener() {

                            @Override
                            public void onShow(DialogInterface dialog) {
                                SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                text.setGravity(Gravity.CENTER);
                                alertDialog.setCancelable(false);
                                alertDialog.setTitleText("PIN baru berbeda dengan PIN konfirmasi!");
                                alertDialog.setConfirmText("Ok");
                                alertDialog.setConfirmClickListener(null);
                            }
                        });

                        loading.show();
                        pinBaru1.setError("");
                        pinBaru2.setError("");
                        return;

                    }
                    else {

                        PIN = pinLama.getText().toString();
                        PIN_BARU1 = pinBaru1.getText().toString();
                        PIN_BARU2 = pinBaru2.getText().toString();
                        KONTEN_SMS = FORMAT_SMS + " " + PIN + " " + PIN_BARU1 + " " +PIN_BARU2;

                        sendSMS(NO_HP,KONTEN_SMS);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        SMSReceiver.bindListener(new SMSListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void messageReceived(String messageText) {

                final Matcher matcher = Pattern.compile("disertai digit").matcher(messageText);

                if(matcher.find()){
                    AlertDialog.Builder alert = new AlertDialog.Builder(GantiPIN.this);
                    alert.setMessage(messageText); //Message here

                    LinearLayout layout = new LinearLayout(GantiPIN.this);
                    layout.setOrientation(LinearLayout.VERTICAL);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(20, 0, 30, 0);

                    final EditText input = new EditText(GantiPIN.this);
                    layout.addView(input,params);
                    input.setInputType(InputType.TYPE_CLASS_TEXT);


                    input.setMaxLines(5);
                    input.setMaxWidth(50);
                    input.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5) {

                    }});


                    alert.setView(layout);


                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            responseKONTEN = input.getText().toString();

                            if (input.getText().length()==0){
                                input.setError("masukan data dengan benar");
                                return;
                            }else {

                                sendSMS(NO_HP,responseKONTEN);
                                return;
                            }


                        } // End of onClick(DialogInterface dialog, int whichButton)
                    }); //End of alert.setPositiveButton


                    alert.setCancelable(false);
                    alert.show();
                    return;

                }else{
                    new SweetAlertDialog(GantiPIN.this, SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText("")
                            .setContentText(messageText)
                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    return;
                }

            }
        });
                

        
    }

    @Override
    protected void onPause() {
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        registerReceiver(sendBroadcastReceiver,intentFilter);
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        super.onDestroy();
    }

    public void ClearField(){
        pinLama.getText().clear();
        pinBaru1.getText().clear();
        pinBaru2.getText().clear();

    }

    private void sendSMS(String phoneNumber, String message) {

        loading = new SweetAlertDialog(GantiPIN.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setTitleText("Memproses...");

        loading.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                text.setGravity(Gravity.CENTER);
            }
        });
        loading.show();


        new CountDownTimer(30000, 1000) { //40000 milli seconds is total time, 1000 milli seconds is time interval

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                loading.dismiss();
            }
        }.start();




        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
                SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

        registerReceiver(deliverBroadcastReceiver, new IntentFilter(DELIVERED));
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);



    }

    BroadcastReceiver sendBroadcastReceiver = new SentReceiver();
    BroadcastReceiver deliverBroadcastReceiver = new DeliverReceiver();


    class DeliverReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
ClearField();
                    loading.dismiss();
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(getBaseContext(), "Gagal Terkirimm",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }


    class SentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "Mengirim", Toast.LENGTH_SHORT)
                            .show();
                    ClearField();
                    loading.dismiss();
                    new SweetAlertDialog(GantiPIN.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Perhatian !")
                            .setContentText("Mohon menunggu sampai anda mendapat informasi balasan dari kami !")

                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Toast.makeText(getBaseContext(), "Generic failure",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Toast.makeText(getBaseContext(), "No service",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT)
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Toast.makeText(getBaseContext(), "Radio off",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }






}
