package bankkalteng.mobile.com.smsbanking;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

/**
 * Created by rpj on 22/02/17.
 */

public class SMSReceiver extends BroadcastReceiver {
    private static SMSListener mListener;
    private String sendeR;
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle data  = intent.getExtras();

        Object[] pdus = (Object[]) data.get("pdus");

        for(int i=0;i<pdus.length;i++){
            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);

            String sender = smsMessage.getDisplayOriginatingAddress();

            String messageBody = smsMessage.getMessageBody();

            if(sender.equalsIgnoreCase("3125")){
                mListener.messageReceived(messageBody);
            }


        }
    }

    public static void bindListener(SMSListener listener){
        mListener=listener;
    }
}
