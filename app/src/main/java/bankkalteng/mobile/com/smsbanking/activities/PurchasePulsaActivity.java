package bankkalteng.mobile.com.smsbanking.activities;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.text.method.KeyListener;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import bankkalteng.mobile.com.smsbanking.R;
import bankkalteng.mobile.com.smsbanking.SMSListener;
import bankkalteng.mobile.com.smsbanking.SMSReceiver;

/**
 * Created by MOBILEDEV on 1/19/2017.
 */
public class PurchasePulsaActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS =0 ;
    public static final String PREFS_NAME = "App_Pref";
    EditText txt_noHP;
    int selectedIdRbDenom;
    ImageView imgProv;
    TextView lbl_denom;
    String prefixProvider,x;
    SharedPreferences sharedPreferences;
    Button btnSubmit,btnIndex;
    ProgressDialog pDialog;
    SweetAlertDialog sweetAlertDialog;
    String defaultAccountType,defaultProtocol;
    Animation animation,animations;
    AutoCompleteTextView txtSourceAcct ,txtDenom;
    String responseKONTEN, defaultrekIndex, NO_HP,FORMAT_SMS,REK,KONTEN_SMS,ORDER_ID,NOMINAL,ussdCode;
    RadioGroup rgBolt, rgIM3, rgMentari, rgSmart, rgThree, rgTelkomsel, rgXL, selectedRadioGroup;
    CharSequence digitNo;

    IntentFilter intentFilter;
    SweetAlertDialog loading;

    String[] list_prefix = {"0812", "0813","0851",
    "0852",
    "0853",
    "0821",
    "0822",
    "0823",
    "0856",
    "0857",
    "0814",
    "0858",
    "0815",
    "0816",
    "0831",
    "0832",
    "0833",
    "0838",
    "0817",
    "0818",
    "0819",
    "0859",
    "0877",
    "0878",
    "0879",
    "0895",
    "0896",
    "0897",
    "0881",
    "0882",
    "0883",
    "0884",
    "0887",
    "0888",
    "0889",
    "999"};
    
    
    String[] sourceAcct={"1","2","3","4","5"};
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        intentFilter = new IntentFilter();
        intentFilter.addAction("SMS_RECEIVED_ACTION");
        NO_HP =getResources().getString(R.string.NO_HP);
        FORMAT_SMS = getResources().getString(R.string.SMS_PEMBELIAN_PULSA);




        setContentView(R.layout.purchase_pulsa_layout_);
        rgBolt = (RadioGroup)findViewById(R.id.rgBolt);
        rgIM3 = (RadioGroup)findViewById(R.id.rgIM3);
        rgMentari= (RadioGroup)findViewById(R.id.rgMentari);
        rgSmart = (RadioGroup)findViewById(R.id.rgSmartfren);
        rgTelkomsel = (RadioGroup)findViewById(R.id.rgTelkomsel);
        rgThree = (RadioGroup)findViewById(R.id.rgThree);
        rgXL = (RadioGroup)findViewById(R.id.rgXl);

        rgXL.setVisibility(View.GONE);
        rgSmart.setVisibility(View.GONE);
        rgThree.setVisibility(View.GONE);
        rgMentari.setVisibility(View.GONE);
        rgTelkomsel.setVisibility(View.GONE);
        rgBolt.setVisibility(View.GONE);
        rgIM3.setVisibility(View.GONE);




        lbl_denom = (TextView)findViewById(R.id.lbl_denom);
        lbl_denom.setVisibility(View.GONE);





        btnIndex = (Button)findViewById(R.id.btnIndex);
        btnSubmit = (Button)findViewById(R.id.btnSubmit);
        txt_noHP = (EditText)findViewById(R.id.txt_nohp);
        imgProv = (ImageView) findViewById(R.id.img_prov);
        txtSourceAcct = (AutoCompleteTextView)findViewById(R.id.txt_sorceacct);
//        txtDenom = (AutoCompleteTextView)findViewById(R.id.txt_denom);
        defaultrekIndex = "1";
        sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        defaultAccountType= sharedPreferences.getString("typeAkun", "");
        defaultProtocol = sharedPreferences.getString("jalurProtocol", "");
        txt_noHP.setImeOptions(EditorInfo.IME_ACTION_DONE);
        animation = AnimationUtils.loadAnimation(this, R.anim.push_left);
        animation.reset();
        animations = AnimationUtils.loadAnimation(this, R.anim.push_right);
        animations.reset();
        imgProv.setImageResource(R.mipmap.ic_simcard);
        imgProv.clearAnimation();
        imgProv.startAnimation(animation);
//        ArrayAdapter adapters = new ArrayAdapter(this,android.R.layout.simple_list_item_1,denom);
//        txtDenom.setAdapter(adapters);
//        txtDenom.setThreshold(1);

        if(defaultAccountType.equalsIgnoreCase("Satu Akun")){
            txtSourceAcct.setFilters(new InputFilter[]{new InputFilter.LengthFilter(35) {

            }});
            txtSourceAcct.setText("index rekening 1");
            txtSourceAcct.setTextSize(14);
            txtSourceAcct.setClickable(false);
            txtSourceAcct.setCursorVisible(false);
            txtSourceAcct.setEnabled(false);


        }else{
            ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,sourceAcct);
            txtSourceAcct.setAdapter(adapter);
            txtSourceAcct.setThreshold(1);
            txtSourceAcct.setFilters(new InputFilter[]{new InputFilter.LengthFilter(1) {

            }});
        }

        txtSourceAcct.setInputType(InputType.TYPE_CLASS_PHONE);
        txt_noHP.setInputType(InputType.TYPE_CLASS_PHONE);
        txt_noHP.setFilters(new InputFilter[]{new InputFilter.LengthFilter(14) {

        }});
//        txtDenom.setInputType(InputType.TYPE_CLASS_PHONE);

        KeyListener keyListener = DigitsKeyListener.getInstance("1234567890");
        txtSourceAcct.setKeyListener(keyListener);
        txt_noHP.setKeyListener(keyListener);
//        txtDenom.setKeyListener(keyListener);
        digitNo = txt_noHP.getText();

        btnIndex.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                KONTEN_SMS = getResources().getString(R.string.SMS_INDEX_REKENING);
                sendSMS(NO_HP,KONTEN_SMS);
            }
        });


        
        
        
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    if(defaultAccountType.equalsIgnoreCase("Satu Akun")){

                        if(defaultProtocol.equalsIgnoreCase("SMS")){
                            if (txt_noHP.getText().toString().equals("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(PurchasePulsaActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan no hp !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                txt_noHP.setError("");
                                return;
                            }if (txt_noHP.getText().length()<10) {
                                SweetAlertDialog loading = new SweetAlertDialog(PurchasePulsaActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan no hp dengan benar !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                txt_noHP.setError("");
                                return;
                            }if(selectedRadioGroup.getCheckedRadioButtonId() == -1){

                                SweetAlertDialog loading = new SweetAlertDialog(PurchasePulsaActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Silahkan pilih nominal pulsa !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                return;
                            }if(txt_noHP.getText().toString().length() < 10){
                                SweetAlertDialog loading = new SweetAlertDialog(PurchasePulsaActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan no hp dengan benar !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                txt_noHP.setError("");
                                return;
                            }

                            else {

                                selectedIdRbDenom = selectedRadioGroup.getCheckedRadioButtonId();
                                RadioButton radioButton = (RadioButton) findViewById(selectedIdRbDenom);

                                String finalDenom;
                                finalDenom = radioButton.getText().toString();
                                if(finalDenom.equalsIgnoreCase("20rb")){
                                    NOMINAL = "20000";
                               }if(finalDenom.equalsIgnoreCase("25rb")){
                                    NOMINAL = "25000";
                                }if(finalDenom.equalsIgnoreCase("30rb")){
                                    NOMINAL = "30000";
                                }if(finalDenom.equalsIgnoreCase("50rb")){
                                    NOMINAL = "50000";
                                }if(finalDenom.equalsIgnoreCase("100rb")){
                                    NOMINAL = "100000";
                                }if(finalDenom.equalsIgnoreCase("150rb")){
                                    NOMINAL = "150000";
                                }if(finalDenom.equalsIgnoreCase("200rb")){
                                    NOMINAL = "200000";
                                }else {

                                    ORDER_ID = txt_noHP.getText().toString();
                                    REK = defaultrekIndex;
                                    KONTEN_SMS = FORMAT_SMS + " " + REK + " " + ORDER_ID + " " + NOMINAL;
                                    sendSMS(NO_HP, KONTEN_SMS);

                                }
                            }
                        }else{
                            ussdCode = "*" + "141" + "*" + "125" + Uri.encode("#");
                            if (ActivityCompat.checkSelfPermission(PurchasePulsaActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            if (ActivityCompat.checkSelfPermission(PurchasePulsaActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
                        }


                    }else{


                        if(defaultProtocol.equalsIgnoreCase("SMS")) {
                            if (txtSourceAcct.getText().toString().equals("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(PurchasePulsaActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan index rekening !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                txtSourceAcct.setError("");
                                return;
                            }
                            if (txt_noHP.getText().toString().equals("")) {
                                SweetAlertDialog loading = new SweetAlertDialog(PurchasePulsaActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan no hp !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                txt_noHP.setError("");
                                return;
                            }if (txt_noHP.getText().length()<10) {
                                SweetAlertDialog loading = new SweetAlertDialog(PurchasePulsaActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan no hp dengan benar !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                txt_noHP.setError("");
                                return;
                            }if(selectedRadioGroup.getCheckedRadioButtonId() == -1){

                                new SweetAlertDialog(PurchasePulsaActivity.this, SweetAlertDialog.WARNING_TYPE)
                                        .setTitleText("Silahkan pilih nominal pulsa !")
                                        .setConfirmText("Ok")
                                        .setConfirmClickListener(null)
                                        .show();
                                return;
                            }if(txt_noHP.getText().toString().length() < 10){
                                SweetAlertDialog loading = new SweetAlertDialog(PurchasePulsaActivity.this, SweetAlertDialog.WARNING_TYPE);

                                loading.setOnShowListener(new DialogInterface.OnShowListener() {

                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                                        TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                                        text.setTextSize(TypedValue.COMPLEX_UNIT_SP,12);
                                        text.setGravity(Gravity.CENTER);
                                        alertDialog.setCancelable(false);
                                        alertDialog.setTitleText("Masukan no hp dengan benar !");
                                        alertDialog.setConfirmText("Ok");
                                        alertDialog.setConfirmClickListener(null);
                                    }
                                });

                                loading.show();
                                txt_noHP.setError("");
                                return;
                            }

                            else {

                                selectedIdRbDenom = selectedRadioGroup.getCheckedRadioButtonId();
                                RadioButton radioButton = (RadioButton) findViewById(selectedIdRbDenom);

                                String finalDenom;
                                finalDenom = radioButton.getText().toString();
                                if(finalDenom.equalsIgnoreCase("20rb")){
                                    NOMINAL = "20000";
                                }if(finalDenom.equalsIgnoreCase("25rb")){
                                    NOMINAL = "25000";
                                }if(finalDenom.equalsIgnoreCase("30rb")){
                                    NOMINAL = "30000";
                                }if(finalDenom.equalsIgnoreCase("50rb")){
                                    NOMINAL = "50000";
                                }if(finalDenom.equalsIgnoreCase("100rb")){
                                    NOMINAL = "100000";
                                }if(finalDenom.equalsIgnoreCase("150rb")){
                                    NOMINAL = "150000";
                                }if(finalDenom.equalsIgnoreCase("200rb")){
                                    NOMINAL = "200000";
                                }else {


                                    ORDER_ID = txt_noHP.getText().toString();
                                    REK = txtSourceAcct.getText().toString();
                                    KONTEN_SMS = FORMAT_SMS + " " + REK + " " + ORDER_ID + " " + NOMINAL;
                                    sendSMS(NO_HP, KONTEN_SMS);

                                }

                            }
                        }else{
                            ussdCode = "*" + "141" + "*" + "5" + "*"  + "1" + "*" + REK + "*" + ORDER_ID + "*" + NOMINAL + Uri.encode("#");
                            if (ActivityCompat.checkSelfPermission(PurchasePulsaActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            if (ActivityCompat.checkSelfPermission(PurchasePulsaActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        });


        txt_noHP.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                
                x = s.toString();


                    if(x.length() == 4){
                       try{

                            if(x.equalsIgnoreCase("0812") || x.equalsIgnoreCase("0813") || x.equalsIgnoreCase("0851") ||
                                    x.equalsIgnoreCase("0852") || x.equalsIgnoreCase("0853") || x.equalsIgnoreCase("0821") ||
                                    x.equalsIgnoreCase("0822") || x.equalsIgnoreCase("0823")){
                                lbl_denom.setVisibility(View.VISIBLE);
                                selectedRadioGroup = rgTelkomsel;
                                rgXL.setVisibility(View.GONE);
                                rgSmart.setVisibility(View.GONE);
                                rgThree.setVisibility(View.GONE);
                                rgMentari.setVisibility(View.GONE);
                                rgTelkomsel.setVisibility(View.VISIBLE);
                                rgBolt.setVisibility(View.GONE);
                                rgIM3.setVisibility(View.GONE);

                                imgProv.setImageResource(R.mipmap.ic_telkomsel);
                                imgProv.clearAnimation();
                                imgProv.startAnimation(animation);

                            }if(x.equalsIgnoreCase("0856") || x.equalsIgnoreCase("0857") || x.equalsIgnoreCase("0814")) {
                               lbl_denom.setVisibility(View.VISIBLE);
                                selectedRadioGroup = rgIM3;
                                rgXL.setVisibility(View.GONE);
                                rgSmart.setVisibility(View.GONE);
                                rgThree.setVisibility(View.GONE);
                                rgMentari.setVisibility(View.GONE);
                                rgTelkomsel.setVisibility(View.GONE);
                                rgBolt.setVisibility(View.GONE);
                                rgIM3.setVisibility(View.VISIBLE);
                                imgProv.setImageResource(R.mipmap.ic_im3);
                                imgProv.clearAnimation();
                                imgProv.startAnimation(animation);

                            }
                            if(x.equalsIgnoreCase("0858") || x.equalsIgnoreCase("0815") || x.equalsIgnoreCase("0816")) {
                                lbl_denom.setVisibility(View.VISIBLE);
                                selectedRadioGroup = rgMentari;
                                rgXL.setVisibility(View.GONE);
                                rgSmart.setVisibility(View.GONE);
                                rgThree.setVisibility(View.GONE);
                                rgMentari.setVisibility(View.VISIBLE);
                                rgTelkomsel.setVisibility(View.GONE);
                                rgBolt.setVisibility(View.GONE);
                                rgIM3.setVisibility(View.GONE);
                                imgProv.setImageResource(R.mipmap.ic_indosat_mentari);
                                imgProv.clearAnimation();
                                imgProv.startAnimation(animation);

                            }
                            if(x.equalsIgnoreCase("0831") || x.equalsIgnoreCase("0832") || x.equalsIgnoreCase("0833") ||
                                    x.equalsIgnoreCase("0838") || x.equalsIgnoreCase("0817") || x.equalsIgnoreCase("0818")
                                    || x.equalsIgnoreCase("0819") || x.equalsIgnoreCase("0859") || x.equalsIgnoreCase("0877") || x.equalsIgnoreCase("0878") || x.equalsIgnoreCase("0879")) {
                                lbl_denom.setVisibility(View.VISIBLE);
                                selectedRadioGroup = rgXL;
                                rgXL.setVisibility(View.VISIBLE);
                                rgSmart.setVisibility(View.GONE);
                                rgThree.setVisibility(View.GONE);
                                rgMentari.setVisibility(View.GONE);
                                rgTelkomsel.setVisibility(View.GONE);
                                rgBolt.setVisibility(View.GONE);
                                rgIM3.setVisibility(View.GONE);
                                imgProv.setImageResource(R.mipmap.ic_xl);
                                imgProv.clearAnimation();
                                imgProv.startAnimation(animation);

                            }if(x.equalsIgnoreCase("0895") || x.equalsIgnoreCase("0896") || x.equalsIgnoreCase("0897") ||
                                    x.equalsIgnoreCase("0898") || x.equalsIgnoreCase("0899")) {
                               lbl_denom.setVisibility(View.VISIBLE);
                                selectedRadioGroup = rgThree;
                                rgXL.setVisibility(View.GONE);
                                rgSmart.setVisibility(View.GONE);
                                rgThree.setVisibility(View.VISIBLE);
                                rgMentari.setVisibility(View.GONE);
                                rgTelkomsel.setVisibility(View.GONE);
                                rgBolt.setVisibility(View.GONE);
                                rgIM3.setVisibility(View.GONE);
                                imgProv.setImageResource(R.mipmap.ic_three);
                                imgProv.clearAnimation();
                                imgProv.startAnimation(animation);

                            }if(x.equalsIgnoreCase("0881") || x.equalsIgnoreCase("0882") || x.equalsIgnoreCase("0883") ||
                                    x.equalsIgnoreCase("0884") || x.equalsIgnoreCase("0887") || x.equalsIgnoreCase("0888") ||
                                    x.equalsIgnoreCase("0889")) {
                               lbl_denom.setVisibility(View.VISIBLE);
                                selectedRadioGroup = rgSmart;
                                rgXL.setVisibility(View.GONE);
                                rgSmart.setVisibility(View.VISIBLE);
                                rgThree.setVisibility(View.GONE);
                                rgMentari.setVisibility(View.GONE);
                                rgTelkomsel.setVisibility(View.GONE);
                                rgBolt.setVisibility(View.GONE);
                                rgIM3.setVisibility(View.GONE);
                                imgProv.setImageResource(R.mipmap.ic_smart);
                                imgProv.clearAnimation();
                                imgProv.startAnimation(animation);

                            }else{

                            }
                        }catch (Exception e){
                            e.printStackTrace();

                        }
                    }if(x.length() == 3){
                            try {
                                if (x.equalsIgnoreCase("999")) {
                                    lbl_denom.setVisibility(View.VISIBLE);
                                    selectedRadioGroup = rgBolt;
                                    rgXL.setVisibility(View.GONE);
                                    rgSmart.setVisibility(View.GONE);
                                    rgThree.setVisibility(View.GONE);
                                    rgMentari.setVisibility(View.GONE);
                                    rgTelkomsel.setVisibility(View.GONE);
                                    rgBolt.setVisibility(View.VISIBLE);
                                    rgIM3.setVisibility(View.GONE);
                                    imgProv.setImageResource(R.mipmap.ic_bolt);
                                    imgProv.clearAnimation();
                                    imgProv.startAnimation(animation);

                                }
                            }catch (Exception e){
                                    e.printStackTrace();
                                }


                    }if(x.equalsIgnoreCase("")){
                    lbl_denom.setVisibility(View.GONE);
                    rgXL.setVisibility(View.GONE);
                    rgSmart.setVisibility(View.GONE);
                    rgThree.setVisibility(View.GONE);
                    rgMentari.setVisibility(View.GONE);
                    rgTelkomsel.setVisibility(View.GONE);
                    rgBolt.setVisibility(View.GONE);
                    rgIM3.setVisibility(View.GONE);
                    imgProv.setImageResource(R.mipmap.ic_simcard);
                    imgProv.clearAnimation();
                    imgProv.startAnimation(animation);




                }
                // TODO Auto-generated method stub
            }
        });


        SMSReceiver.bindListener(new SMSListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void messageReceived(String messageText) {

                final Matcher matcher = Pattern.compile("disertai digit").matcher(messageText);
                if(matcher.find()){
                    AlertDialog.Builder alert = new AlertDialog.Builder(PurchasePulsaActivity.this);
                    alert.setMessage(messageText); //Message here

                    LinearLayout layout = new LinearLayout(PurchasePulsaActivity.this);
                    layout.setOrientation(LinearLayout.VERTICAL);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(20, 0, 30, 0);

                    final EditText input = new EditText(PurchasePulsaActivity.this);
                    layout.addView(input,params);
                    input.setInputType(InputType.TYPE_CLASS_TEXT);


                    input.setMaxLines(5);
                    input.setMaxWidth(50);
                    input.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    input.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5) {

                    }});


                    alert.setView(layout);


                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            responseKONTEN = input.getText().toString();

                            if (input.getText().length()==0){
                                input.setError("masukan index rekening");
                                return;
                            }else {

                                sendSMS(NO_HP,responseKONTEN);
                                return;
                            }


                        } // End of onClick(DialogInterface dialog, int whichButton)
                    }); //End of alert.setPositiveButton


                    alert.setCancelable(false);
                    alert.show();
                    return;
                }else{

                    new SweetAlertDialog(PurchasePulsaActivity.this, SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText("")
                            .setContentText(messageText)
                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    return;
                }

            }
        });



    }

    private void valiadsiPrefix(String no){
        try {
            if (no.length() >= 4){
                try{
                    if(no.equalsIgnoreCase("0812") || no.equalsIgnoreCase("0813") || no.equalsIgnoreCase("0851") ||
                                    no.equalsIgnoreCase("0852") || x.equalsIgnoreCase("0853") || x.equalsIgnoreCase("0821") ||
                                    x.equalsIgnoreCase("0822") || x.equalsIgnoreCase("0823")) {
                        selectedRadioGroup = rgTelkomsel;
                        rgXL.setVisibility(View.GONE);
                        rgSmart.setVisibility(View.GONE);
                        rgThree.setVisibility(View.GONE);
                        rgMentari.setVisibility(View.GONE);
                        rgTelkomsel.setVisibility(View.VISIBLE);
                        rgBolt.setVisibility(View.GONE);
                        rgIM3.setVisibility(View.GONE);

                        imgProv.setImageResource(R.mipmap.ic_telkomsel);
                        imgProv.clearAnimation();
                        imgProv.startAnimation(animation);
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }



    
    public void ClearField(){
        if(defaultAccountType.equalsIgnoreCase("Satu Akun")){
            txtSourceAcct.setFilters(new InputFilter[]{new InputFilter.LengthFilter(35) {

            }});
            txtSourceAcct.setText("Anda menggunakan index rekening 1");
            txtSourceAcct.setTextSize(12);
            txtSourceAcct.setClickable(false);
            txtSourceAcct.setCursorVisible(false);
            txtSourceAcct.setEnabled(false);
        }else{
            txtSourceAcct.getText().clear();
        }

        lbl_denom.setVisibility(View.GONE);
        txt_noHP.getText().clear();
//        selectedRadioGroup.clearCheck();
        rgXL.setVisibility(View.GONE);
        rgSmart.setVisibility(View.GONE);
        rgThree.setVisibility(View.GONE);
        rgMentari.setVisibility(View.GONE);
        rgTelkomsel.setVisibility(View.GONE);
        rgBolt.setVisibility(View.GONE);
        rgIM3.setVisibility(View.GONE);

        imgProv.setImageResource(R.mipmap.ic_simcard);
        imgProv.clearAnimation();
        imgProv.startAnimation(animation);
    }





    @Override
    protected void onPause() {
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        registerReceiver(sendBroadcastReceiver,intentFilter);
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(sendBroadcastReceiver);
            unregisterReceiver(deliverBroadcastReceiver);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        super.onDestroy();
    }


    private void sendSMS(String phoneNumber, String message) {

        loading = new SweetAlertDialog(PurchasePulsaActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.setCancelable(false);
        loading.setTitleText("Memproses...");

        loading.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                SweetAlertDialog alertDialog = (SweetAlertDialog) dialog;
                TextView text = (TextView) alertDialog.findViewById(R.id.title_text);
                text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                text.setGravity(Gravity.CENTER);
            }
        });
        loading.show();


        new CountDownTimer(30000, 1000) { //40000 milli seconds is total time, 1000 milli seconds is time interval

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                loading.dismiss();
            }
        }.start();




        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(
                SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        registerReceiver(sendBroadcastReceiver, new IntentFilter(SENT));

        registerReceiver(deliverBroadcastReceiver, new IntentFilter(DELIVERED));
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
        ClearField();


    }

    BroadcastReceiver sendBroadcastReceiver = new SentReceiver();
    BroadcastReceiver deliverBroadcastReceiver = new DeliverReceiver();


    class DeliverReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
//                    Toast.makeText(getBaseContext(), "Mengirim",
//                            Toast.LENGTH_SHORT).show();
                    loading.dismiss();
                    break;
                case Activity.RESULT_CANCELED:
                    Toast.makeText(getBaseContext(), "Gagal Mengirim",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }


    class SentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent arg1) {
            switch (getResultCode()) {
                case Activity.RESULT_OK:
                    Toast.makeText(getBaseContext(), "TERKIRIM", Toast.LENGTH_SHORT)
                            .show();
                    loading.dismiss();
                    new SweetAlertDialog(PurchasePulsaActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Perhatian !")
                            .setContentText("Mohon menunggu sampai anda mendapat informasi balasan dari kami !")

                            .setConfirmText("OK")
                            .showCancelButton(true)
                            .setCancelClickListener(null)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                    Toast.makeText(getBaseContext(), "Generic failure",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NO_SERVICE:
                    Toast.makeText(getBaseContext(), "No service",
                            Toast.LENGTH_SHORT).show();
                    break;
                case SmsManager.RESULT_ERROR_NULL_PDU:
                    Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT)
                            .show();
                    break;
                case SmsManager.RESULT_ERROR_RADIO_OFF:
                    Toast.makeText(getBaseContext(), "Radio off",
                            Toast.LENGTH_SHORT).show();
                    break;
            }

        }
    }

}
