package bankkalteng.mobile.com.smsbanking.activities;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import bankkalteng.mobile.com.smsbanking.R;

/**
 * Created by MOBILEDEV on 10/27/2016.
 */
public class TransferInhouseActivityIndex extends AppCompatActivity {
    public static final String PREFS_NAME = "App_Pref";
    AutoCompleteTextView sourceAccount, benefAccount;
    SharedPreferences sharedPreferences;
    EditText nominal,berita,benef;
    Button btnSubmit;
    RelativeLayout lyt_1, lyt_2, lyt_3;
    TextView lbl_index_rek;
    private ProgressDialog pDialog;
    String INDEX_REK1, INDEX_REK2,BERITA, FORMAT_SMS,KONTEN_SMS,NOMINAL, NO_HP, NO_REK_TUJUAN, defaultAccountType,defaultProtocol,ussdCode;

    String[] sourceAcct={"1","2","3","4","5"};
    String[] BenefAcct={"1","2","3","4","5"};

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inhouse_transfer_layout);
        sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        defaultAccountType= sharedPreferences.getString("typeAkun", "");
        defaultProtocol = sharedPreferences.getString("jalurProtocol", "");
        FORMAT_SMS= getResources().getString(R.string.SMS_TRANSFER_INHOUSE);
        NO_HP =getResources().getString(R.string.NO_HP);
        lyt_1 = (RelativeLayout)findViewById(R.id.lyt_1);

        lbl_index_rek = (TextView)findViewById(R.id.lbl_index_rek);
        sourceAccount=(AutoCompleteTextView)findViewById(R.id.autoCompleteTextView1);
//        benefAccount=(AutoCompleteTextView)findViewById(R.id.autoCompleteTextView2);
        nominal =(EditText)findViewById(R.id.txt_nominal);
        benef =(EditText)findViewById(R.id.txt_benef);
        berita =(EditText)findViewById(R.id.txt_berita);
        btnSubmit = (Button)findViewById(R.id.btnSubmit);
        if(defaultAccountType.equalsIgnoreCase("satu akun")){
            lyt_1.setVisibility(View.GONE);
            lyt_1.setClickable(false);
            lbl_index_rek.setVisibility(View.VISIBLE);
            lbl_index_rek.setText("INDEX REKENING = 1");
        }else{
            lyt_1.setVisibility(View.VISIBLE);
            lyt_1.setClickable(true);
            lbl_index_rek.setVisibility(View.GONE);
        }
        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,sourceAcct);
        sourceAccount.setAdapter(adapter);
        sourceAccount.setThreshold(1);

//        ArrayAdapter adapters = new ArrayAdapter(this,android.R.layout.simple_list_item_1,BenefAcct);
//        benefAccount.setAdapter(adapters);
//        benefAccount.setThreshold(1);

        final InputMethodManager imm = (InputMethodManager)getApplication().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(nominal.getWindowToken(), 0);

        nominal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                imm.showSoftInputFromInputMethod(nominal.getWindowToken(), 0);
            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if(defaultAccountType.equalsIgnoreCase("satu akun")){
                        if(defaultProtocol.equalsIgnoreCase("SMS")){
                            if (benef.getText().toString().equals("")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(TransferInhouseActivityIndex.this);
                                builder.setMessage("Harap masukan rekening tujuan anda !")
                                        .setTitle("Transfer")
                                        .setPositiveButton(android.R.string.ok, null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                                return;
                            }if (nominal.getText().toString().equals("")){
                                AlertDialog.Builder builder = new AlertDialog.Builder(TransferInhouseActivityIndex.this);
                                builder.setMessage("Harap masukan nominal transfer !")
                                        .setTitle("Transfer")
                                        .setPositiveButton(android.R.string.ok, null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                                return;
                            }else {
                                INDEX_REK2 = benef.getText().toString();
                                NOMINAL = nominal.getText().toString();


                                BERITA = berita.getText().toString();
                                if(BERITA.equalsIgnoreCase("")){
                                    KONTEN_SMS = FORMAT_SMS + " " + "1" + " " + INDEX_REK2 + " " + NOMINAL;
                                }else{
                                    KONTEN_SMS = FORMAT_SMS + " " + "1" + " " + INDEX_REK2 + " " + NOMINAL + "#" + BERITA;
                                }

                                kirimSMS(NO_HP,KONTEN_SMS);
                            }
                        }else{
                            ussdCode = "*" + "141" + "*" + "125" +  Uri.encode("#");
                            if (ActivityCompat.checkSelfPermission(TransferInhouseActivityIndex.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            if (ActivityCompat.checkSelfPermission(TransferInhouseActivityIndex.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
                        }


                    }else{
                        if(defaultProtocol.equalsIgnoreCase("SMS")) {

                            if (benef.getText().toString().equals("")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(TransferInhouseActivityIndex.this);
                                builder.setMessage("Harap masukan rekening tujuan anda !")
                                        .setTitle("Transfer")
                                        .setPositiveButton(android.R.string.ok, null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                                return;
                            }
                            if (nominal.getText().toString().equals("")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(TransferInhouseActivityIndex.this);
                                builder.setMessage("Harap masukan nominal transfer !")
                                        .setTitle("Transfer")
                                        .setPositiveButton(android.R.string.ok, null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                                return;
                            } else {
                                INDEX_REK1 = sourceAccount.getText().toString();
                                INDEX_REK2 = benef.getText().toString();
                                NOMINAL = nominal.getText().toString();
                                BERITA = berita.getText().toString();
                                if(BERITA.equalsIgnoreCase("")){
                                    KONTEN_SMS = FORMAT_SMS + " " + "1" + " " + INDEX_REK2 + " " + NOMINAL;
                                }else{
                                    KONTEN_SMS = FORMAT_SMS + " " + "1" + " " + INDEX_REK2 + " " + NOMINAL + "#" + BERITA;
                                }

                                kirimSMS(NO_HP,KONTEN_SMS);
                            }
                        }else{
                            ussdCode = "*" + "141" + "*" + "125" + Uri.encode("#");
                            if (ActivityCompat.checkSelfPermission(TransferInhouseActivityIndex.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            if (ActivityCompat.checkSelfPermission(TransferInhouseActivityIndex.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return;
                            }
                            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }



            }

        });
    }

    private void kirimSMS(String nohp, String konten){
        pDialog = ProgressDialog.show( TransferInhouseActivityIndex.this, "SMS Banking",
                "sedang memproses ...", true);
        String SENT = "dikirim";
        String DELIVERED = "diterima";
//        Toast.makeText(
//                this.getApplicationContext(),"KONTEN SMS= " +nohp + " " + konten, Toast.LENGTH_LONG).show();
        Intent sentIntent = new Intent(SENT);
     /*Create Pending Intents*/
        PendingIntent sentPI = PendingIntent.getBroadcast(
                TransferInhouseActivityIndex.this, 0, sentIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Intent deliveryIntent = new Intent(DELIVERED);

        PendingIntent deliverPI = PendingIntent.getBroadcast(
                TransferInhouseActivityIndex.this, 0, deliveryIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
     /* Register for SMS send action */
        registerReceiver(new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                String result = "";

                switch (getResultCode()) {

                    case Activity.RESULT_OK:
                        result = "SMS BERHASIL DIKIRIM";
                        ClearField();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        result = "SMS GAGAL DIKIRIM";
                        ClearField();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        result = "SINYAL ERROR";
                        ClearField();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        result = "PDU tidak tersedia";
                        ClearField();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        result = "TIDAK ADA JARINGAN";
                        ClearField();
                        break;
                }

                Toast.makeText(TransferInhouseActivityIndex.this, result,
                        Toast.LENGTH_LONG).show();
                pDialog.dismiss();
            }

        }, new IntentFilter(SENT));
     /* Register for Delivery event */
        registerReceiver(new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
//                            Toast.makeText(getApplicationContext(), "Deliverd",
//                                    Toast.LENGTH_LONG).show();

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        TransferInhouseActivityIndex.this);

                // set title
                alertDialogBuilder.setTitle("SMS Banking");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Silahkan cek pesan masuk anda")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
//                                openInbox();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }

        }, new IntentFilter(DELIVERED));

      /*Send SMS*/
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(nohp, null, konten, sentPI,
                deliverPI);





    }

    public void ClearField(){
        sourceAccount.getText().clear();
        benefAccount.getText().clear();
        nominal.getText().clear();
        berita.getText().clear();
    }

    public void openInbox() {
        String application_name = "com.android.mms";
        try {
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.LAUNCHER");

            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            List<ResolveInfo> resolveinfo_list = this.getPackageManager()
                    .queryIntentActivities(intent, 0);

            for (ResolveInfo info : resolveinfo_list) {
                if (info.activityInfo.packageName
                        .equalsIgnoreCase(application_name)) {
                    launchComponent(info.activityInfo.packageName,
                            info.activityInfo.name);
                    break;
                }
            }
        } catch (ActivityNotFoundException e) {
            Toast.makeText(
                    this.getApplicationContext(),
                    "There was a problem loading the application: "
                            + application_name, Toast.LENGTH_SHORT).show();
        }
    }

    private void launchComponent(String packageName, String name) {
        Intent launch_intent = new Intent("android.intent.action.MAIN");
        launch_intent.addCategory("android.intent.category.LAUNCHER");
        launch_intent.setComponent(new ComponentName(packageName, name));
        launch_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(launch_intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id != null) {
//            return true;
//        }
        return super.onOptionsItemSelected(item);
    }
}
